import Levenshtein
import sys

def find_most_similar(word, dictionary):
    min_dist = sys.maxsize
    result = None
    for w in dictionary:
        dist = Levenshtein.distance(word,w)
        if dist < min_dist:
            result = w
            min_dist = dist
    return result


def find_similars(word, dictionary, max_dist=2):
    result = []
    for w in dictionary:
        dist = Levenshtein.distance(word,w)
        if dist < max_dist:
            result.appenf(w)
    return result