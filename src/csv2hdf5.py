# -*- coding: utf-8 -*-

import sys
import os
import bigcsv
import csv
import getopt
import tables
import ConfigParser
import datetime
import time

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter

#EPOCH = datetime.datetime.strptime('1/1/70', "%d/%m/%y")

def seconds_since_epoch(value):
    return int(time.mktime(value))
    #return (value - EPOCH).total_seconds()

#HDF_NULL_DATETIME = seconds_since_epoch(time.strptime('1/1/1970', "%d/%m/%Y"))
#print HDF_NULL_DATETIME

print ("CSV to HDF")
bigcsv.powered_by_massa()

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "", [])
    except getopt.GetoptError as err:
        print str(err)
        print "Uso basico: python csv2hdf5.py"
        print "\t-h para ajuda"
        exit()

    columns_formats = {} # formato para colunas datetime

    csv_file_name = argv[0]
    param_file_name = os.path.splitext(csv_file_name)[0] + ".param"
    hdf_file_name = os.path.splitext(csv_file_name)[0] + ".h5"

    param = ConfigParser.ConfigParser()
    param.read(param_file_name)
    hdf = tables.open_file(hdf_file_name, 'w')

    header = param.get('MAIN', 'header').split("|")

    dialect = bigcsv.build_dialect_from_param(param)
    #bigcsv.print_dialect_params(dialect)

    description_name = {}

    for i, c in enumerate(header):
        coltype = param.get(c, "column_type")
        if coltype == bigcsv.COLTYPE_STRING:
            max_length = int(param.get(c, "max_length"))
            description_name[c] = tables.StringCol(max_length, dflt='', pos=i)
        elif coltype == bigcsv.COLTYPE_INT:
            description_name[c] = tables.Int32Col(pos=i) # TODO: Usar tipos especificos dependendo do range max/min
        elif coltype == bigcsv.COLTYPE_LONG:
            description_name[c] = tables.Int64Col(pos=i) # TODO: Usar tipos especificos dependendo do range max/min
        elif coltype == bigcsv.COLTYPE_FLOAT:
            description_name[c] = tables.FloatCol(pos=i)
        elif coltype == bigcsv.COLTYPE_BOOLEAN:
            description_name[c] = tables.BoolCol(pos=i)
        elif coltype == bigcsv.COLTYPE_DATETIME:
            description_name[c] = tables.Time32Col(pos=i)
            columns_formats[i] = param.get(c, "column_format")
        elif coltype == bigcsv.COLTYPE_DATE:
            description_name[c] = tables.Time32Col(pos=i) # TODO: Usar tipos especificos
            columns_formats[i] = param.get(c, "column_format")
        elif coltype == bigcsv.COLTYPE_TIME:
            description_name[c] = tables.Time64Col(pos=i) # TODO: Usar tipos especificos
            columns_formats[i] = param.get(c, "column_format")

    print(description_name)

    group = hdf.create_group("/", 'input', 'arquivos de entrada')
    table = hdf.create_table(group, 'ass', description_name, "valores importados do CSV")
    row = table.row

    update_interval = 1000
    counter = update_interval

    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=int(param.get('MAIN', 'num_rows')))
    pbar.start()
    with bigcsv.smartopen(csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if param.get('MAIN', 'has_header') == 'True':
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = update_interval
            for i, l in enumerate(line):
                coltype = param.get(header[i], "column_type")
                if coltype == bigcsv.COLTYPE_STRING:
                    row[header[i]] = l
                elif coltype == bigcsv.COLTYPE_INT:
                    row[header[i]] = bigcsv.try_parse_int(l, 0)
                elif coltype == bigcsv.COLTYPE_LONG:
                    row[header[i]] = bigcsv.try_parse_long(l, 0)
                elif coltype == bigcsv.COLTYPE_FLOAT:
                    row[header[i]] = bigcsv.try_parse_float(l, 0)
                elif coltype == bigcsv.COLTYPE_BOOLEAN:
                    row[header[i]] = bigcsv.try_parse_boolean(l)
                elif coltype == bigcsv.COLTYPE_DATETIME:
                    try:
                        row[header[i]] = seconds_since_epoch(time.strptime(l, columns_formats[i]))
                    except ValueError:
                        row[header[i]] = 0
                elif coltype == bigcsv.COLTYPE_DATE:
                    row[header[i]] = seconds_since_epoch(time.strptime(l, columns_formats[i]))
                elif coltype == bigcsv.COLTYPE_TIME:
                    # row[header[i]] = seconds_since_epoch(datetime.datetime.strptime(l, columns_formats[i]))
                    pass
            row.append()

    pbar.finish()
    hdf.close()
    print ("\n*** FIM ***")


if __name__ == "__main__":
    main(sys.argv[1:])