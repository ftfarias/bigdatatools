# -*- coding: utf-8 -*-

import sys
import os
import bigcsv
import csv
import argparse
import ConfigParser
from progressbar import ProgressBar


def main(args):
    print ("Stats Basic")
    bigcsv.powered_by_massa()

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.csv_file_name)[0] + ".param"

    if not os.path.isfile(args.csv_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.csv_file_name))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.csv_file_name))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.csv_file_name))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)
    #print(columns_params)

    print ("Total of rows: {total:,}".format(total=total_rows))

    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
    pbar.start()
    counter = args.update_interval
    with bigcsv.smartopen(args.csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            parsed_line = []
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval

            # constructs the line with the correct types
            for i, l in enumerate(line):
                # parse the value to the right type
                value = bigcsv.to_type(l, columns_params[header[i]])
                # add to the parsed line
                parsed_line.append(value)

            # Do the job here
            #print parsed_line

    pbar.finish()
    print ("\n*** FIM ***")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('csv_file_name', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()
    main(args)