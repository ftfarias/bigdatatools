# -*- coding: utf-8 -*-

import argparse
import sqlite3


def main():
    parser = argparse.ArgumentParser(description='Arquivo texto com QUERY')

    parser.add_argument('file_sql', help='Nome do arquivo com os comandos SQL')
    parser.add_argument('sqlite', help='Nome do arquivo que contem o BD')

    args = parser.parse_args()

    file_sql = open(args.file_sql, "r")
    conn = sqlite3.connect(args.sqlite)

    for query in file_sql:
        print query
        c = conn.cursor()
        c.execute(query)

        conn.commit()
        conn.close()


if __name__ == "__main__":
    main()