# -*- coding: utf-8 -*-

import csv
from csv import Dialect
import datetime
import sys
import os
import bz2
import random
from collections import namedtuple
import re

'''
Lendo do arquivo: modelo.full.csv
100% [##############################################################################################] Registros: 40,485,283 Elapsed Time: 1:27:50 Time: 1:27:50  7.68 krows/s

*** FIM ***

real    87m50.264s
user    33m43.299s
sys     10m24.965s
'''

from _csv import Error, QUOTE_MINIMAL, QUOTE_ALL, QUOTE_NONNUMERIC, QUOTE_NONE

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter

widgets = [Percentage(), ' ', Bar(marker='#', left='[', right=']'), \
           ' Registros:', ' ', Counter(),' ', Timer(),  ' ',\
           AdaptiveETA(), ' ', FileTransferSpeed(unit="rows")]

ColumnParam = namedtuple('ColumnParam', ['index', 'name', 'column_type', 'function', 'format'])
CsvMetadata = namedtuple('CsvMetadata', ['filename', 'dialect', 'total_rows', 'has_header', 'header', 'columns_params'])


COLTYPE_STRING = "string"
COLTYPE_NULL = "NULL"
COLTYPE_INT = "int"
COLTYPE_LONG = "long"
COLTYPE_BOOLEAN = "bool"
COLTYPE_FLOAT = "float"
COLTYPE_DATE = "date"
COLTYPE_TIME = "time"
COLTYPE_DATETIME = "datetime"

COLFUNC_KEY = "key"  # códigos, identificadores
COLFUNC_FACTOR = "factor" # fatores
COLFUNC_VALUE = "value" # valores
COLFUNC_VALUE_AGE = "value_age"  # idade
COLFUNC_VALUE_YEAR = "value_year"  # ano
COLFUNC_VALUE_MONTH = "value_month"  # ano

#Levels of Measurement:
#Nominal, Ordinal, Interval and Ratio
COLLEVEL_NOMINAL = "NOMINAL"
COLLEVEL_ORDINAL = "ORDINAL"
COLLEVEL_INTERVAL = "INTERVAL"
COLLEVEL_RATIO = "RATIO"
COLLEVEL_OTHER = "OTHER"

bigcsv2sqlite = {
    COLTYPE_STRING: 'TEXT',
    COLTYPE_INT: 'INTEGER',
    COLTYPE_LONG: 'INTEGER',
    COLTYPE_FLOAT: 'REAL',
    COLTYPE_BOOLEAN: 'TEXT',
    COLTYPE_DATE: 'DATE',
    COLTYPE_TIME: 'DATETIME',
    COLTYPE_DATETIME: 'DATETIME',
    COLTYPE_NULL: 'TEXT'
}

bigcsv2postgres = {
    COLTYPE_STRING: 'varchar',
    COLTYPE_INT: 'bigint',
    COLTYPE_LONG: 'bigint',
    COLTYPE_FLOAT: 'real',
    COLTYPE_BOOLEAN: 'boolean',
    COLTYPE_DATE: 'date',
    COLTYPE_TIME: 'time',
    COLTYPE_DATETIME: 'timestamp',
    COLTYPE_NULL: 'char(1)'
}

NULL_DATETIME = datetime.datetime.strptime('1/1/1900 0:0:0', "%d/%m/%Y %H:%M:%S")


def clean_string(text):
    rx = re.compile('\W+')
    return rx.sub('', text).strip()


def smartopen(file_name, mode):
    file_extension = os.path.splitext(file_name)[1]
    if file_extension == '.bz2':
        return bz2.BZ2File(file_name, mode)
    return open(file_name, mode)


def read_column(cp, total_rows, csv_file_name, has_header, update_interval, dialect):
    """
    Read the column <cp> from a the file <csv_file_name>  and returns the values as an array
    :param cp: the ColumnParatemer structure
    :param total_rows:
    :param csv_file_name:
    :param has_header:
    :param update_interval:
    :param dialect:
    :return:
    """
    pbar = ProgressBar(widgets=widgets, maxval=total_rows)
    pbar.start()
    result = []
    counter = update_interval
    with smartopen(csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = update_interval

            col_value = to_type(line[cp.index], cp)
            if col_value is not None:
                result.append(col_value)

    pbar.finish()
    print("Column loaded, size: {s:,} kbytes".format(s=sys.getsizeof(result)/1024))
    return result


def read_columns(col_names, columns_param, total_rows, csv_file_name, has_header, update_interval, dialect,
                 remove_nones=False):  # cp - ColumnParam
    pbar = ProgressBar(widgets=widgets, maxval=total_rows)
    pbar.start()
    result = [[] for i in col_names ]
    counter = update_interval
    with smartopen(csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = update_interval
                total = 0

            for i, col_name in enumerate(col_names):
                cp = columns_param[col_name]
                col_value = to_type(line[cp.index], cp)
                if remove_nones:
                    if col_value is not None:
                        result[i].append(col_value)
                else:
                    result[i].append(col_value)

    pbar.finish()
    for col_name in enumerate(col_names):
        total += sys.getsizeof(result[i])
    print("Loaded, memory size: {s:,} kbytes".format(s=total/1024))
    #print("Column loaded, size: {s:,} kbytes".format(s=sys.getsizeof(result)/1024))
    return result



def read_factor_from_columns(col_names, meta, remove_nones=False, update_interval=10000):  # cp - ColumnParam
    """
    Returns a list of
    :param col_names:
    :param meta:
    :param remove_nones:
    :param update_interval:
    :return:
    """
    pbar = ProgressBar(widgets=widgets, maxval=meta.total_rows)
    pbar.start()
    counter = meta.update_interval
    result = dict([(i,set()) for i in col_names ])
    with smartopen(meta.csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=meta.dialect)

        if meta.has_header:
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = update_interval
                total = 0

            for i, col_name in enumerate(col_names):
                cp = meta.columns_params[col_name]
                result[i].add_contact(line[cp.index])

    pbar.finish()
    return result

# todo
def readcsv(file_name, update_interval=10, sniffingsize=10000, verbose=True):
    with open(file_name, 'rb') as csvfile:
        if verbose:
            print("Sniffing CSV for dialect...")
        dialect = csv.Sniffer().sniff(csvfile.read(sniffingsize))
        if verbose:
            print("Detected:")
            print_dialect_params(dialect)
        counter = update_interval
        if verbose:
            print("\nStarting parsing....")

        csvfile.seek(0)
        csv_reader = csv.reader(csvfile, dialect=dialect)
        header = csv_reader.next()
        record = namedtuple('EmployeeRecord', header, verbose=True)
        for emp in map(record._make, csv_reader):
            counter -= 1
            if verbose and counter == 0:
                sys.stdout.write("\rLines: {lines:,}".format(lines=csv_reader.line_num))
                sys.stdout.flush()
                counter = update_interval
            print emp

#todo
def read_dict_csv(file_name, update_interval=1000, sniffingsize=100, verbose=False):
    with open(file_name, 'rb') as csvfile:
        if verbose:
            print("Sniffing {file} for dialect...".format(file=file_name))
        dialect = csv.Sniffer().sniff(csvfile.read(sniffingsize))
        if verbose:
            print("Detected:")
            print_dialect_params(dialect)
        counter = update_interval
        if verbose:
            print("\nStarting parsing....")

        csvfile.seek(0)
        csv_reader = csv.reader(csvfile, dialect=dialect)
        header = csv_reader.next()
        table = {}
        for emp in csv_reader:
            counter -= 1
            if verbose and counter == 0:
                sys.stdout.write("\rLines: {lines:,}".format(lines=csv_reader.line_num))
                sys.stdout.flush()
                counter = update_interval
            table[emp[0]] = emp[1]
        return table


def csv_metadata_from_param(csv_file_name, param):
    dialect, total_rows, has_header, header, columns_params = read_columns_from_param(param)
    return CsvMetadata(csv_file_name, dialect, total_rows, has_header, header, columns_params)


def read_columns_from_param(param):
    header = param.get('MAIN', 'header').split("|")
    dialect = build_dialect_from_param(param)
    total_rows = int(param.get('MAIN', 'num_rows'))
    has_header = param.get('MAIN', 'has_header') == 'True'

    columns_params = {}

    for i, col_name in enumerate(header):
        column_type = param.get(col_name, "column_type")
        if param.has_option(col_name, 'column_format'):
            column_format = param.get(col_name, 'column_format')
        else:
            column_format = None

        if param.has_option(col_name, 'column_function'):
            column_function = param.get(col_name, 'column_function')
        else:
            column_function = None
        columns_params[col_name] = ColumnParam(i, col_name, column_type, column_function, column_format)

    return dialect, total_rows, has_header, header, columns_params


def column_print(cp, prefix="Column"):  # cp - ColumnParam
        if cp.format is not None:
            print("{pre} {col} at index {idx}, type {ty}, function {f}, format {form}".format(pre=prefix,
                                                                                col=cp.name, idx=cp.index,
                                                                                f=cp.function,
                                                                                ty=cp.column_type, form=cp.format))
        else:
            print("{pre} {col} at index {idx}, type {ty}, function {f}".format(pre=prefix, col=cp.name, idx=cp.index,
                                                                 f=cp.function, ty=cp.column_type))


def powered_by_massa():
    print ("\n--- Powered by MASSA™ Engine ---")
    print (random.sample(['[M]assive', '[M]egalomaniac', '[M]ixable', '[M]ultiple', '[M]ultivarieted'], 1)[0])
    print (random.sample(['[a]uto-regulated', '[a]dvanced', '[a]syncronous', '[a]diabatic', '[a]natomic'], 1)[0])
    s = ['[s]elf-adaptative', '[s]urealistic', '[s]yncronous', '[s]ymmetric',
         '[s]istematic', '[s]obrenatural', '[s]uperflous', '[s]tatistical',
         '[s]ocratic', '[s]ofismatic', '[s]igmoidal']
    random.shuffle(s)
    print (s[0])
    print (s[1])
    print ("[a]nalytic")
    print ("Engine")
    print ("")


def print_dialect_params(dialect):
    print ("Delimiter           : {d}".format(d=dialect.delimiter))
    print ("Quote Char          : {d}".format(d=dialect.quotechar))
    print ("Escape Char         : {d}".format(d=dialect.escapechar))
    print ("Double Quote        : {d}".format(d=dialect.doublequote))
    print ("Skip Initial Space  : {d}".format(d=dialect.skipinitialspace))
    print ("Line Terminator     : {d}".format(d=dialect.lineterminator.encode('string_escape')))
    print ("Quoting             : {d}".format(d=dialect.quoting))

def build_dialect_from_param(param):
    delimiter = param.get("MAIN", "delimiter")
    quotechar = param.get("MAIN", "quotechar")
    escapechar = param.get("MAIN", "escapechar")
    doublequote = param.get("MAIN", "doublequote") == 'True'
    skipinitialspace = param.get("MAIN", "skipinitialspace") == 'True'
    lineterminator = param.get("MAIN", "lineterminator").replace('\\n', '\n').replace('\\r', '\r')
    quoting = int(param.get("MAIN", "quoting"))
    class dialect(Dialect):
        _name = "sniffed"
        lineterminator = '\r\n'
        quoting = QUOTE_MINIMAL
        # escapechar = ''

    dialect.doublequote = doublequote
    dialect.delimiter = delimiter
    dialect.escapechar = escapechar if escapechar != 'None' else None
    dialect.doublequote = doublequote
    # _csv.reader won't accept a quotechar of ''
    dialect.quotechar = quotechar or '"'
    dialect.skipinitialspace = bool(skipinitialspace)

    return dialect

NULL_VALUES = ('na', 'n/a', 'none', 'null', '.')
TRUE_VALUES = ('1', 'yes', 'y', 'true', 't', 's', 'sim')
FALSE_VALUES = ('0', 'no', 'n', 'false', 'f', "nao", "não")
#DATE_FORMATS = ('%d/%m/%Y', '%Y/%m/%d')
DATE_FORMATS = ('%Y/%m/%d',)
TIME_FORMATS = ('%H:%M:%S',)
DATETIME_FORMATS = ('%Y-%m-%d %H:%M:%S', '%Y/%m/%d %H:%M:%S')


def to_type(value, cp, error_value=None): # cp - ColumnParam
    if cp.column_type == COLTYPE_STRING:
        return value

    elif cp.column_type == COLTYPE_INT:
        try:
            return int(value)
        except ValueError:
            return error_value

    elif cp.column_type == COLTYPE_LONG:
        try:
            return long(value)
        except ValueError:
            return error_value

    elif cp.column_type == COLTYPE_FLOAT:
        try:
            return float(value)
        except ValueError:
            return error_value

    elif cp.column_type == COLTYPE_BOOLEAN:
        return try_parse_boolean(value)

    elif cp.column_type == COLTYPE_DATETIME:
        try:
            return datetime.datetime.strptime(value, cp.format)
        except ValueError:
            return error_value

    elif cp.column_type == COLTYPE_DATE:
        try:
            return datetime.datetime.strptime(value, cp.format)
        except ValueError:
            return error_value

    elif cp.column_type == COLTYPE_TIME:
        try:
            return datetime.datetime.strptime(value, cp.format)
        except ValueError:
            return error_value
    elif cp.column_type == COLTYPE_NULL:
        return error_value
    else:
        print("Tipo de campo desconhecido: {c}".format(c=cp.column_type))


def parse_boolean(valor,
                  true_values=TRUE_VALUES,
                  false_values=FALSE_VALUES):
    valor = valor.lower()
    if valor in true_values:
        return True
    elif valor in false_values:
        return False
    else:
        return None


def try_parse_boolean(value, iferror=False):
    v = parse_boolean(value)
    if v is None:
        return iferror
    else:
        return v

def try_parse_int(value, iferror=0):
    try:
        return int(value)
    except ValueError:
        return iferror

def try_parse_long(value, iferror=0):
    try:
        return long(value)
    except ValueError:
        return iferror

def try_parse_float(value, iferror=0):
    try:
        return float(value)
    except ValueError:
        return iferror

def try_parse_datetime(value, mask, iferror=NULL_DATETIME):
    try:
        return datetime.datetime.strptime(value, mask);
    except ValueError:
        return iferror


def parse_type(valor,
                date_formats=DATE_FORMATS,
                time_formats=TIME_FORMATS,
                datetime_formats=DATETIME_FORMATS,
                null_values=NULL_VALUES,
                true_values=TRUE_VALUES,
                false_values=FALSE_VALUES):

    result = []

    if not valor or valor is None:
        result.append(COLTYPE_NULL)

    valor = valor.lower()

    if valor in null_values:
        result.append(COLTYPE_NULL)
    elif valor in true_values:
        result.append(COLTYPE_BOOLEAN)
    elif valor in false_values:
        result.append(COLTYPE_BOOLEAN)

    # try as int
    try:
        v = int(valor)
        result.append(COLTYPE_INT)
    except (ValueError, OverflowError):
        pass

    # try as long
    try:
        v = long(valor)
        result.append(COLTYPE_LONG)
    except (ValueError, OverflowError):
        pass

    # try as float
    try:
        v = float(valor)
        result.append(COLTYPE_FLOAT)
    except (ValueError, OverflowError):
        pass

    # if we already have candidates, don't need to keep parsing
    if result:
        return result

    if len(valor) >= 6:  # can't have dates/times with less than 6 characteres
        # try as date
        for datetime_format in datetime_formats:
            try:
                v = datetime.datetime.strptime(valor, datetime_format)
                return ["{type}/{format}".format(type=COLTYPE_DATETIME, format=datetime_format)]
            except (ValueError, OverflowError):
                pass

        for date_format in date_formats:
            try:
                v = datetime.datetime.strptime(valor, date_format)
                return ["{type}/{format}".format(type=COLTYPE_DATE, format=date_format)]
            except (ValueError, OverflowError):
                pass

        for time_format in time_formats:
            try:
                v = datetime.datetime.strptime(time_format, date_format)
                return ["{type}/{format}".format(type=COLTYPE_TIME, format=time_format)]
            except (ValueError, OverflowError):
                pass

    return [COLTYPE_STRING]

def guess_type_from_type_counts(counter, total_of_rows):
    nulls = counter[COLTYPE_NULL]
    # total of VALID field
    total_of_valid = float(total_of_rows - nulls)
    c_str = counter[COLTYPE_STRING] / total_of_valid
    c_int = counter[COLTYPE_INT] / total_of_valid
    c_long = counter[COLTYPE_LONG] / total_of_valid
    c_float = counter[COLTYPE_FLOAT] / total_of_valid
    c_boolean = counter[COLTYPE_BOOLEAN] / total_of_valid

    if (c_boolean) > 0.95 and counter[COLTYPE_BOOLEAN] == counter[COLTYPE_INT]:
        return COLTYPE_BOOLEAN

    if (c_int > 0.95):
        return COLTYPE_INT

    if (c_long) > 0.95:
        return COLTYPE_LONG

    if (c_float) > 0.95:
        return COLTYPE_FLOAT

    for k in counter.elements():
        if 1.0 * counter[k] / total_of_valid > 0.95:
            return k

    # I don' know... string?
    return COLTYPE_STRING


def max_values(values):
    #print(values)
    max_val = max(values.values())
    #print(max_val)
    return [(k,values[k]) for k in values.keys() if values[k] == max_val]

# Guard Sniffer's type checking against builds that exclude complex()
try:
    complex
except NameError:
    complex = float

class Sniffer:
    '''
    "Sniffs" the format of a CSV file (i.e. delimiter, quotechar)
    Returns a Dialect object.
    '''
    def __init__(self):
        # in case there is more than one possible delimiter
        self.preferred = [',', '\t', ';', ' ', ':']


    def sniff(self, sample, delimiters=None):
        """
        Returns a dialect (or None) corresponding to the sample
        """

        quotechar, doublequote, delimiter, skipinitialspace = \
                   self._guess_quote_and_delimiter(sample, delimiters)
        if not delimiter:
            delimiter, skipinitialspace = self._guess_delimiter(sample,
                                                                delimiters)
        if not delimiter:
            raise Error, "Could not determine delimiter"

        class dialect(Dialect):
            _name = "sniffed"
            lineterminator = '\r\n'
            quoting = QUOTE_MINIMAL
            # escapechar = ''

        dialect.doublequote = doublequote
        dialect.delimiter = delimiter
        # _csv.reader won't accept a quotechar of ''
        dialect.quotechar = quotechar or '"'
        dialect.skipinitialspace = skipinitialspace

        return dialect

    def number_of_columns(self, sample, sniff_size=100):
        StringIO
        return 0

    def _guess_quote_and_delimiter(self, data, delimiters):
        """
        Looks for text enclosed between two identical quotes
        (the probable quotechar) which are preceded and followed
        by the same character (the probable delimiter).
        For example:
                         ,'some text',
        The quote with the most wins, same with the delimiter.
        If there is no quotechar the delimiter can't be determined
        this way.
        """

        matches = []
        for restr in ('(?P<delim>[^\w\n"\'])(?P<space> ?)(?P<quote>["\']).*?(?P=quote)(?P=delim)', # ,".*?",
                      '(?:^|\n)(?P<quote>["\']).*?(?P=quote)(?P<delim>[^\w\n"\'])(?P<space> ?)',   #  ".*?",
                      '(?P<delim>>[^\w\n"\'])(?P<space> ?)(?P<quote>["\']).*?(?P=quote)(?:$|\n)',  # ,".*?"
                      '(?:^|\n)(?P<quote>["\']).*?(?P=quote)(?:$|\n)'):                            #  ".*?" (no delim, no space)
            regexp = re.compile(restr, re.DOTALL | re.MULTILINE)
            matches = regexp.findall(data)
            if matches:
                break

        if not matches:
            # (quotechar, doublequote, delimiter, skipinitialspace)
            return ('', False, None, 0)
        quotes = {}
        delims = {}
        spaces = 0
        for m in matches:
            n = regexp.groupindex['quote'] - 1
            key = m[n]
            if key:
                quotes[key] = quotes.get(key, 0) + 1
            try:
                n = regexp.groupindex['delim'] - 1
                key = m[n]
            except KeyError:
                continue
            if key and (delimiters is None or key in delimiters):
                delims[key] = delims.get(key, 0) + 1
            try:
                n = regexp.groupindex['space'] - 1
            except KeyError:
                continue
            if m[n]:
                spaces += 1

        quotechar = reduce(lambda a, b, quotes = quotes:
                           (quotes[a] > quotes[b]) and a or b, quotes.keys())

        if delims:
            delim = reduce(lambda a, b, delims = delims:
                           (delims[a] > delims[b]) and a or b, delims.keys())
            skipinitialspace = delims[delim] == spaces
            if delim == '\n': # most likely a file with a single column
                delim = ''
        else:
            # there is *no* delimiter, it's a single column of quoted data
            delim = ''
            skipinitialspace = 0

        # if we see an extra quote between delimiters, we've got a
        # double quoted format
        dq_regexp = re.compile(
                               r"((%(delim)s)|^)\W*%(quote)s[^%(delim)s\n]*%(quote)s[^%(delim)s\n]*%(quote)s\W*((%(delim)s)|$)" % \
                               {'delim':re.escape(delim), 'quote':quotechar}, re.MULTILINE)



        if dq_regexp.search(data):
            doublequote = True
        else:
            doublequote = False

        return (quotechar, doublequote, delim, skipinitialspace)


    def _guess_delimiter(self, data, delimiters):
        """
        The delimiter /should/ occur the same number of times on
        each row. However, due to malformed data, it may not. We don't want
        an all or nothing approach, so we allow for small variations in this
        number.
          1) build a table of the frequency of each character on every line.
          2) build a table of frequencies of this frequency (meta-frequency?),
             e.g.  'x occurred 5 times in 10 rows, 6 times in 1000 rows,
             7 times in 2 rows'
          3) use the mode of the meta-frequency to determine the /expected/
             frequency for that character
          4) find out how often the character actually meets that goal
          5) the character that best meets its goal is the delimiter
        For performance reasons, the data is evaluated in chunks, so it can
        try and evaluate the smallest portion of the data possible, evaluating
        additional chunks as necessary.
        """

        data = filter(None, data.split('\n'))

        ascii = [chr(c) for c in range(127)] # 7-bit ASCII

        # build frequency tables
        chunkLength = min(10, len(data))
        iteration = 0
        charFrequency = {}
        modes = {}
        delims = {}
        start, end = 0, min(chunkLength, len(data))
        while start < len(data):
            iteration += 1
            for line in data[start:end]:
                for char in ascii:
                    metaFrequency = charFrequency.get(char, {})
                    # must count even if frequency is 0
                    freq = line.count(char)
                    # value is the mode
                    metaFrequency[freq] = metaFrequency.get(freq, 0) + 1
                    charFrequency[char] = metaFrequency

            for char in charFrequency.keys():
                items = charFrequency[char].items()
                if len(items) == 1 and items[0][0] == 0:
                    continue
                # get the mode of the frequencies
                if len(items) > 1:
                    modes[char] = reduce(lambda a, b: a[1] > b[1] and a or b,
                                         items)
                    # adjust the mode - subtract the sum of all
                    # other frequencies
                    items.remove(modes[char])
                    modes[char] = (modes[char][0], modes[char][1]
                                   - reduce(lambda a, b: (0, a[1] + b[1]),
                                            items)[1])
                else:
                    modes[char] = items[0]

            # build a list of possible delimiters
            modeList = modes.items()
            total = float(chunkLength * iteration)
            # (rows of consistent data) / (number of rows) = 100%
            consistency = 1.0
            # minimum consistency threshold
            threshold = 0.9
            while len(delims) == 0 and consistency >= threshold:
                for k, v in modeList:
                    if v[0] > 0 and v[1] > 0:
                        if ((v[1]/total) >= consistency and
                            (delimiters is None or k in delimiters)):
                            delims[k] = v
                consistency -= 0.01

            if len(delims) == 1:
                delim = delims.keys()[0]
                skipinitialspace = (data[0].count(delim) ==
                                    data[0].count("%c " % delim))
                return (delim, skipinitialspace)

            # analyze another chunkLength lines
            start = end
            end += chunkLength

        if not delims:
            return ('', 0)

        # if there's more than one, fall back to a 'preferred' list
        if len(delims) > 1:
            for d in self.preferred:
                if d in delims.keys():
                    skipinitialspace = (data[0].count(d) ==
                                        data[0].count("%c " % d))
                    return (d, skipinitialspace)

        # nothing else indicates a preference, pick the character that
        # dominates(?)
        items = [(v,k) for (k,v) in delims.items()]
        items.sort()
        delim = items[-1][1]

        skipinitialspace = (data[0].count(delim) ==
                            data[0].count("%c " % delim))
        return (delim, skipinitialspace)

    def has_header(self, csv_reader, sniff_size=100):
        # Creates a dictionary of types of data in each column. If any
        # column is of a single type (say, integers), *except* for the first
        # row, then the first row is presumed to be labels. If the type
        # can't be determined, it is assumed to be a string in which case
        # the length of the string is the determining factor: if all of the
        # rows except for the first are the same length, it's a header.
        # Finally, a 'vote' is taken at the end for each column, adding or
        # subtracting from the likelihood of the first row being a header.

        header = csv_reader.next() # assume first row is header

        columns = len(header)
        column_types = {}
        for i in range(columns):
            column_types[i] = None

        checked = 0
        for row in csv_reader:
            # arbitrary number of rows to check, to keep it sane
            if checked > sniff_size:
                break
            checked += 1

            if len(row) != columns:
                continue # skip rows that have irregular number of columns

            for col in column_types.keys():

                for thisType in [int, long, float, complex]:
                    try:
                        thisType(row[col])
                        break
                    except (ValueError, OverflowError):
                        pass
                else:
                    # fallback to length of string
                    thisType = len(row[col])

                # treat longs as ints
                if thisType == long:
                    thisType = int

                if thisType != column_types[col]:
                    if column_types[col] is None: # add new column type
                        column_types[col] = thisType
                    else:
                        # type is inconsistent, remove column from
                        # consideration
                        del column_types[col]

        # finally, compare results against first row and "vote"
        # on whether it's a header
        hasHeader = 0
        for col, colType in column_types.items():
            if type(colType) == type(0): # it's a length
                if len(header[col]) != colType:
                    hasHeader += 1
                else:
                    hasHeader -= 1
            else: # attempt typecast
                try:
                    colType(header[col])
                except (ValueError, TypeError):
                    hasHeader += 1
                else:
                    hasHeader -= 1

        return hasHeader > 0
