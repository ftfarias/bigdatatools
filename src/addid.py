import sys

with open(sys.argv[1]) as f:
    counter = 0
    print 'ID,' + f.readline().strip('\n')
    for line in f:
        print str(counter) + ',' + line.strip('\n')
        counter += 1
