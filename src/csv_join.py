# -*- coding: utf-8 -*-

import sys
import os
import cPickle as pickle
import bigcsv
import csv
import argparse
import ConfigParser

from progressbar import ProgressBar


def main(args):
    print ("CSV Join")
    bigcsv.powered_by_massa()

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.input_csv)[0] + ".param"

    if not os.path.isfile(args.input_csv):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.input_csv))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.input_csv))
        exit()

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)
    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)

    print ("Total of rows: {total:,}".format(total=total_rows))

    print("Colunas chave p/ join: "+str(args.key_columns.split(",")))
    key_columns_idx = [header.index(c) for c in args.key_columns.split(",")]
    print("Indices chave p/ join: "+str(key_columns_idx))

    # Join CSV File
    join_file_name = os.path.splitext(args.join_file)[0] + ".csv"
    if not os.path.isfile(join_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=join_file_name))
        exit()

    print('Abrindo arquivo Join CSV: {file}'.format(file=join_file_name))
    join_csv = open(join_file_name, "rb")

    # Join Param File
    join_param_file_name = os.path.splitext(args.join_file)[0] + ".param"

    print('Abrindo arquivo Join Param: {file}'.format(file=join_param_file_name))
    join_param = ConfigParser.ConfigParser()
    join_param.read(join_param_file_name)
    join_dialect, join_total_rows, join_has_header, join_header, join_columns_params = bigcsv.read_columns_from_param(join_param)

    # IDX File
    idx_file_name = os.path.splitext(args.join_file)[0] + ".idx"
    if not os.path.isfile(idx_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=idx_file_name))
        exit()

    print('Abrindo arquivo IDX: {file}'.format(file=idx_file_name))
    with open(idx_file_name,"rb") as idx_file:
        indices = pickle.load(idx_file)
    print('IDX carregado')

    # Arquivo de Saida
    print('Abrindo arquivo de saida: {file}'.format(file=args.output_csv))
    output_file = open(args.output_csv, 'w')
    output_csv_writer = csv.writer(output_file, dialect=dialect)

    # CSV de entrada
    print('Abrindo arquivo CSV: {file}'.format(file=args.input_csv))

    counter = args.update_interval
    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
    pbar.start()
    with bigcsv.smartopen(args.input_csv, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            header = csv_reader.next()
            header += join_header
            output_csv_writer.writerow(header)

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval

            index_join = "|".join([line[i] for i in key_columns_idx])

            pos_inicio = indices[index_join][0]
            join_csv.seek(pos_inicio, 0)
            join_line = join_csv.readline().strip().split(join_dialect.delimiter)

            line += join_line
            output_csv_writer.writerow(line)

    pbar.finish()
    output_file.close()
    print ("\n*** FIM ***")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('input_csv', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('output_csv', help='Nome do arquivo CSV com a saida do processamento')

    parser.add_argument('join_file', help='Nome do arquivo OUT/IDX com a tabela a ser anexada')

    parser.add_argument('key_columns', help='Colunas do arquivo input que servem de chave')

    #parser.add_argument('join_out_columns', help='Colunas do arquivo join ')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()
    main(args)
