# -*- coding: utf-8 -*-

import sys
import codecs

REPLACE = {
    u'á': u'a',
    u'ã': u'a',
    u'é': u'e',
    u'í': u'i',
    u'ó': u'o',
    u'õ': u'o',
    u'ú': u'u',
    u'ç': u'c'
}


def clean_string(s):
    for s_from, s_to in REPLACE.items():
        s = s.replace(s_from, s_to)
    return s


REMOVE = [' do ', ' da ', ' de ']
def remove(s):
    for stop_word in REMOVE:
        s = s.replace(stop_word, ' ')
    return s


def spaces_to_underscore(s):
    s = " ".join(s.split(" "))
    return s.replace(" ", "_")

campos = []
tamanho = []
arquivo = codecs.open(sys.argv[1], 'r', 'utf8')
# arquivo = open(sys.argv[1], 'r')
for l in arquivo:
    linha = l.strip().split('\t')
    nome_campo = linha[0].lower()
    #print(nome_campo)
    nome_campo = clean_string(nome_campo)
    #print(nome_campo)
    nome_campo = remove(nome_campo)
    #print(nome_campo)
    nome_campo = spaces_to_underscore(nome_campo)
    #print(nome_campo)
    campos.append(nome_campo)
    tamanho.append(linha[1])

print('CREATE TABLE xxx (')
for c,t in zip(campos,tamanho):
    print("{nome} varchar({t}), ".format(nome=c,t=t))
print(")")


print('')
print('')
print('')

print('INSERT INTO xxx (')
print(",".join(campos))
print(") values (")
print(','.join(['%s']*len(campos)))
print(")")
