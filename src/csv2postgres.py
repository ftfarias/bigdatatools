# -*- coding: utf-8 -*-

import sys
import os


import bigcsv
import csv
import argparse
import datetime
import ConfigParser
import psycopg2
import codecs
# MAC: export DYLD_LIBRARY_PATH=/Applications/Postgres.app/Contents/Versions/9.3/lib/


# AWS:
# sudo yum install postgresql93
# sudo yum install postgresql93-devel
# export DYLD_LIBRARY_PATH=/usr/lib64/
# sudo -i  pip install psycopg2

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter

#oracle_conn = psycopg2.connect("dbname=ftfarias user=ftfarias")
oracle_conn = psycopg2.connect("host=youfind.cift545shk7k.us-east-1.rds.amazonaws.com"
                               " dbname=ibge user=youfindroot"
                               " password=9ounJZ774qPksWR4U9VrUbbaM9Z8KPgRqMDrLCuL3o4ZFduaKm")

print ("CSV 2 Postgres")
bigcsv.powered_by_massa()

def main(argv):
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('csv_file_name', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('table_name', help='Nome da tabela a ser criada')

    parser.add_argument('--param_file_name',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--drop', '-d', action='store_true',
                        help="Destroi a tabela (drop table) antes de recria-la")

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()

    column_types = []
    column_format = []

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.csv_file_name)[0] + ".param"

    if not os.path.isfile(args.csv_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.csv_file_name))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv2postgres.py {file}"'.format(file=args.csv_file_name))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.csv_file_name))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    print('Tabela: {table}'.format(table=args.table_name))

    header = param.get('MAIN', 'header').split("|")

    for i, c in enumerate(header):
        column_types.append(param.get(c, "column_type"))
        if param.has_option(c, 'column_format'):
            column_format.append(param.get(c, 'column_format'))
        else:
            column_format.append(None)

    campos = []
    for i, c in enumerate(header):
        if column_types[i] == bigcsv.COLTYPE_STRING:
            max_length = param.get(c, 'max_length')
            field_type = 'varchar({n})'.format(n=max_length)
        else:
            field_type = bigcsv.bigcsv2postgres[column_types[i]]
        campos.append("{name} {type} NULL".format(name=c,type=field_type))

    sql_createtable = "CREATE TABLE {table} ( {fields} )".format(table=args.table_name, fields=",".join(campos))

    sql_droptable = "DROP TABLE {table}".format(table=args.table_name)

    sql_insert = "insert into {table} values ({q})".format(table=args.table_name, q=','.join(['%s']*len(header)))

    if args.drop:
        print ("Droping table....")
        cur = oracle_conn.cursor()
        print sql_droptable
        cur.execute(sql_droptable)
        oracle_conn.commit()
        print ("Table dropped !")


    print ("Creating table....")
    cur = oracle_conn.cursor()
    print sql_createtable
    cur.execute(sql_createtable)
    oracle_conn.commit()
    print ("Table created !")

    dialect = bigcsv.build_dialect_from_param(param)
    #bigcsv.print_dialect_params(dialect)

    counter = args.update_interval

    data_buffer = []

    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=int(param.get('MAIN', 'num_rows')))
    pbar.start()
    with bigcsv.smartopen(args.csv_file_name, 'rb') as inputfile:
    #with codecs.open(args.csv_file_name, 'r', 'cp1252') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if param.get('MAIN', 'has_header') == 'True':
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            parsed_line = []
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval
                cur.executemany(sql_insert, data_buffer)
                oracle_conn.commit()
                data_buffer = []
            for i, l in enumerate(line):
                coltype = column_types[i]
                if coltype == bigcsv.COLTYPE_STRING:
                    parsed_line.append(l)
                elif coltype == bigcsv.COLTYPE_INT:
                    try:
                        parsed_line.append(int(l))
                    except:
                        parsed_line.append(0)
                elif coltype == bigcsv.COLTYPE_LONG:
                    parsed_line.append(bigcsv.try_parse_long(l, 0))
                elif coltype == bigcsv.COLTYPE_FLOAT:
                    parsed_line.append(bigcsv.try_parse_float(l, 0))
                elif coltype == bigcsv.COLTYPE_BOOLEAN:
                    parsed_line.append(bigcsv.try_parse_boolean(l))
                elif coltype == bigcsv.COLTYPE_DATETIME:
                    parsed_line.append(bigcsv.try_parse_datetime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_DATE:
                    parsed_line.append(datetime.datetime.strptime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_TIME:
                    parsed_line.append(datetime.datetime.strptime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_NULL:
                    parsed_line.append(None)
                else:
                    print("Tipo de campo desconhecido: {c}".format(c=coltype))
            data_buffer.append(parsed_line)

    pbar.finish()
    cur.executemany(sql_insert, data_buffer)
    oracle_conn.commit()
    oracle_conn.close()
    print ("\n*** FIM ***")

if __name__ == "__main__":
    main(sys.argv[1:])
