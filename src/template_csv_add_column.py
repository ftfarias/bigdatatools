# -*- coding: utf-8 -*-

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import bigcsv
import csv
import argparse
import datetime
import ConfigParser

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter


def main(args):
    print ("CSV AddColumn")
    bigcsv.powered_by_massa()

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.input_csv)[0] + ".param"

    if not os.path.isfile(args.input_csv):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.input_csv))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.input_csv))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.input_csv))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)

    print ("Total of rows: {total:,}".format(total=total_rows))

    output_file = open(args.output_csv, 'w')
    output_csv_writer = csv.writer(output_file, dialect=dialect)

    counter = args.update_interval
    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
    pbar.start()
    with bigcsv.smartopen(args.input_csv, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            header = csv_reader.next()
            header.insert(-2, 'new_col')
            output_csv_writer.writerow(header)

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval

            line.insert(-2, 'ok')
            output_csv_writer.writerow(line)

    pbar.finish()
    output_file.close()
    print ("\n*** FIM ***")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('input_csv', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('output_csv', help='Nome do arquivo CSV com a saida do processamento')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()
    main(args)
