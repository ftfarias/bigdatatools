import pandas as pd
import numpy
import sys

# Coversao dos tipo da Numpy para Postgres
numpy_to_sql = {
    numpy.dtype('int16'): "smallint",
    numpy.dtype('int32'): "integer",
    numpy.dtype('int64'): "bigint",
    numpy.dtype('float64'): "double precision",
    numpy.dtype('object'): "varchar",
}

file_name = sys.argv[1]
table_name = file_name.split(".")[0]

print("Analisando arquivo {name}".format(name=file_name))

df = pd.read_csv(file_name)

sql = ["create table {table_name} (".format(table_name=table_name)]
fields = []

for c in df.columns:
    col_type = df.dtypes[c]
    print(col_type)
    sql_type = numpy_to_sql[col_type]
    if sql_type == 'varchar':
        length = len(max(df[c], key=len))
        sql_type = "varchar({l})".format(l=length)
    fields.append("{c} {tipo}{null}".format(c=c, tipo=sql_type, null=""))

sql.append(", \n".join(fields))
sql.append(")")

print ("\n--------------------------------\n")
print ("\n".join(sql))
print ("\n--------------------------------\n")


# COPY assinatura from '/Users/ftfarias/Projetos/retencao-assinaturas/src/assinatura.csv'