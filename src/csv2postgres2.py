# -*- coding: utf-8 -*-
import pandas

import psycopg2
# add export DYLD_LIBRARY_PATH=/Applications/Postgres.app/Contents/Versions/9.3/lib/ in .bahs_profile

import sys
import bigcsv
import pandas as pd
import numpy
import getopt
import subprocess
from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter

widgets = [Percentage(), ' ', Bar(marker='#', left='[', right=']'), \
           ' Registros:', Counter(), '  ', Timer(), '  ', \
           AdaptiveETA(), '  ', FileTransferSpeed()]


from sqlalchemy import create_engine


# Coversao dos tipo da Numpy para Postgres
numpy_to_sql = {
    numpy.dtype('int16'): "smallint",
    numpy.dtype('int32'): "integer",
    numpy.dtype('int64'): "bigint",
    numpy.dtype('float64'): "double precision",
    numpy.dtype('object'): "varchar",
}

CONN = "postgresql://ftfarias@localhost:5432/ftfarias"

print ("CSV to Postgres")
print ("")
print ("\n--- Powered by MASSA™ Engine ---")
print ("[M]assive")
print ("[a]uto-regulated")
print ("[s]elf-adaptative")
print ("[s]ymmetric")
print ("[a]nalytic ")
print ("Engine")
print ("")


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "", [])
    except getopt.GetoptError as err:
        print str(err)
        print "Uso basico: python timeline_assinatura.py"
        print "\t-h para ajuda"
        exit()

    print ("Timeline Assinatura")
    print ("")
    print ("\n--- Powered by MASSA™ Engine ---")
    print ("[M]assive")
    print ("[a]uto-regulated")
    print ("[s]elf-adaptative")
    print ("[s]urealistic")
    print ("[a]nalytic ")
    print ("Engine")
    print ("")

    print 'Conectanto Postgres ...'
    conn = psycopg2.connect(CONN)
    print 'Conectado Postgres versão %s' % conn


    input_file_name = argv[0]
    table_name = argv[1]

    print ("Copiando  do arquivo {arq} para a tabela {tabela} ".format(arq=input_file_name,tabela=table_name))

    print ('Verificando tamanho do arquivo origem...')
    #size = subprocess.call(['wc', '-l', input_file_name], shell=True)
    size = 100000
    print ('Linhas: {size}'.format(size=size))


    header = []
    cur = conn.cursor()
    sql_insert = ""
    sql_insert_template = "INSERT INTO {table} ({fields}) VALUES {values}"
    try:
        chunksize=10000
        reader = pd.read_csv(input_file_name, chunksize=chunksize, header=0)

        # input_reader = open(input_file_name, "r")
        pbar = ProgressBar(widgets=widgets, maxval=size)
        pbar.start()
        tmp = []
        i = 0
        for chunk in reader:
            #print("chunk: {l}".format(l=len(chunk)))
            #print(chunk)
            for c in chunk.index:
                i += 1
                #print(chunk.iloc[c])

                '''
                if i == 1:
                    header = line
                    print(header)
                    campos = ",".join(header)
                    tipos_valores = ['%s'] * len(header)
                    valores = ",".join(tipos_valores)
                    sql_insert = sql_insert_template.format(table=table_name,fields=campos,values=valores)
                    print(sql_insert)
                    continue
                '''
            #cur.execute(sql_insert, line)
            conn.commit()
        pbar.finish()

    except RuntimeError as ex:
        print("Erro ! {0}".format(ex))

    # fecha as conexoes
    cur.close()
    conn.close()
    print ("\n*** FIM ***")


if __name__ == "__main__":
    main(sys.argv[1:])