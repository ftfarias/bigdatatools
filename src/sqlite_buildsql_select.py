# -*- coding: utf-8 -*-

import sqlite3
import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import ConfigParser
import csv
import bigcsv
from bigcsv import COLTYPE_STRING, COLTYPE_NULL, COLTYPE_INT, COLTYPE_LONG, \
    COLTYPE_BOOLEAN, COLTYPE_FLOAT, COLTYPE_DATE, COLTYPE_TIME, COLTYPE_DATETIME
import biglogging
import collections
import argparse

def main(sqlile_file, table):
    conn = sqlite3.connect(sqlile_file)
    c = conn.cursor()

    c.execute('SELECT * FROM {table} limit 1'.format(table=table))
    c.fetchone()

    for i, col in enumerate(c.description):
        print "{i} -> {col}".format(i=i, col=col)

    print("\nSELECT\n")

    campos = [col[0] for col in c.description]
    print("select {campos} from {table}".format(campos=", ".join(campos),table=table))

    conn.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Select')

    parser.add_argument('table', help='Nome da tabela')

    parser.add_argument('--sqlile_file',
                        help='Nome do arquivo sqlite',
                        default='modelo.sqlite3')

    parser.add_argument('--update_interval', '-u', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=100000)

    parser.add_argument('--sniff_size_types', '-st', type=int,
                        help='Quantos registros devem ser examinados para determinação do tipo. Default = 50000',
                        default=50000)

    parser.add_argument('--sniff_size_dialect', '-sd', type=int,
                        help='Quantos registros devem ser examinados para determinação do dialeto. Default = 100000',
                        default=100000)

    parser.add_argument('--verbose', '-v', action='count', default=1,
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()

    print ("Build Select")
    bigcsv.powered_by_massa()

    main(args.sqlile_file,args.table)