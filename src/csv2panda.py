# -*- coding: utf-8 -*-

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import bigcsv
import csv
import argparse
import datetime
import ConfigParser
import logging
import biglogging
from bigcsv import COLTYPE_STRING, COLTYPE_NULL, COLTYPE_INT, COLTYPE_LONG, \
    COLTYPE_BOOLEAN, COLTYPE_FLOAT, COLTYPE_DATE, COLTYPE_TIME, COLTYPE_DATETIME

logging_path = os.path.join(os.getcwd(), 'log', os.path.splitext(sys.argv[0])[0])
biglogging.initialize_logger(logging_path)
logging.debug('*** START ***')

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter


def create_factor_dict(col, meta):
    counter = args.update_interval
    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=meta.total_rows)
    pbar.start()
    col_meta = meta.columns_params[col]
    col_idx = col_meta.index

    ids = set()

    with bigcsv.smartopen(args.input_csv, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=meta.dialect)
        if meta.has_header:
            csv_reader.next()

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval
            #value = bigcsv.to_type(line[col_idx],col_meta)
            value = line[col_idx]
            ids.add(value)

    pbar.finish()
    value2factor = {}
    for i, value in enumerate(ids):
        value2factor[value] = i
    return value2factor


def main(args):
    print ("CSV AddColumn")
    bigcsv.powered_by_massa()

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.input_csv)[0] + ".param"

    if not os.path.isfile(args.input_csv):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.input_csv))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.input_csv))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.input_csv))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    meta = bigcsv.csv_metadata_from_param(args.input_csv, param)

    print ("Total of rows: {total:,}".format(total=meta.total_rows))

    # cria os fatores
    factor_dicts = {}
    file_parts = os.path.splitext(args.output_csv)
    factor_columns = [col_name for col_name in meta.header if meta.columns_params[col_name].function in
                      [bigcsv.COLFUNC_FACTOR] and meta.columns_params[col_name].type in
                      [COLTYPE_STRING, COLTYPE_DATETIME, COLTYPE_DATE, COLTYPE_NULL, COLTYPE_FLOAT, COLTYPE_BOOLEAN]]
    for col in factor_columns:
        bigcsv.column_print(meta.columns_params[col], "\nFinding factors for")
        factor_dict = create_factor_dict(col, meta)
        factor_dicts[col] = factor_dict
        print('Factors: {f}'.format(f=factor_dict))

        output_file = open("{file}_factor_{factor}.csv".format(file=file_parts[0], factor=col), 'w')
        output_csv_writer = csv.writer(output_file, dialect=meta.dialect)
        for k, v in factor_dict.items():
            output_csv_writer.writerow([v, k])

    output_file = open(args.output_csv, 'w')
    output_csv_writer = csv.writer(output_file, dialect=meta.dialect)

    counter = args.update_interval
    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=meta.total_rows)
    pbar.start()
    col_metas = [meta.columns_params[c] for c in meta.header]
    col_dicts = [factor_dicts.get(c, None) for c in meta.header]

    print("Gerando arquivo de saida ...")
    with bigcsv.smartopen(args.input_csv, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=meta.dialect)

        if meta.has_header:
            header = csv_reader.next()
            result = []
            for i, h in enumerate(header):
                col_meta = col_metas[i]
                if h in factor_columns:
                    result.append("Factor_{h}".format(h=h))
                elif col_meta.function == bigcsv.COLFUNC_FACTOR:
                    result.append(h)
                elif col_meta.function == bigcsv.COLFUNC_VALUE:
                    result.append(h)

            output_csv_writer.writerow(result)

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval

            result = []
            for i, value in enumerate(line):
                col_meta = col_metas[i]
                if col_meta.name in factor_columns:
                    result.append(col_dicts[i][value])
                elif col_meta.function == bigcsv.COLFUNC_FACTOR:
                    result.append(value)
                elif col_meta.function == bigcsv.COLFUNC_VALUE:
                    result.append(value)

            output_csv_writer.writerow(result)

    pbar.finish()
    print ("\n*** FIM ***")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Prepare a CSV for Panda use')

    parser.add_argument('input_csv', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('output_csv', help='Nome do arquivo CSV com a saida do processamento', default=None)

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        )

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()
    main(args)


    # bool com null precisam virar fatores....
    # bool com 'S' e 'N' transformar em 0 e 1