# para instalar o cx_Oracle,
# http://digitalsanctum.com/2007/07/26/installing-oracle-instant-client-on-mac-os-x/

import cx_Oracle
import sys

TABLE = sys.argv[1]
#TABLE = 'DIM_MEIO_RETORNO'

QUERY_DISTINCT = 'select distinct %s, count(*) from %s group by %s order by 2 desc'
QUERY_SELECT_ALL = 'select * from %s'
CONN = 'ast_adm/hVYKKNFTBuvWGNH8YQMEUBfR6oB8tb@assinaturas2.crfgscq6nl7w.sa-east-1.rds.amazonaws.com/orcl'

output = open("Distincts_%s.txt" % TABLE, "w")
con = cx_Oracle.connect(CONN)
print 'Conectado Oracle %s' % con.version

cur = con.cursor()
cur.execute(QUERY_SELECT_ALL % TABLE)
cur.fetchone()
estrutura = cur.description
cur.close()

for i, col in enumerate(estrutura):
    print("Coluna: %d -> %s" % (i, col[0]))
    output.write("# %s\n" % col[0])
    #print(QUERY_DISTINCT % (col[0],TABLE,col[0]))
    cur = con.cursor()
    cur.execute()
    res = cur.fetchall()
    for result in res:
        output.write('{field},{count},{perc}' % (result[0], result[1]))
    cur.close()
    output.flush()


# fecha as conexoes
output.close()
con.close()

