# -*- coding: utf-8 -*-

import sys
import os
import bigcsv
import csv
import getopt
import tables
import ConfigParser
import pandas as pd
import time

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter

#EPOCH = datetime.datetime.strptime('1/1/70', "%d/%m/%y")

def seconds_since_epoch(value):
    return int(time.mktime(value))
    #return (value - EPOCH).total_seconds()

#HDF_NULL_DATETIME = seconds_since_epoch(time.strptime('1/1/1970', "%d/%m/%Y"))
#print HDF_NULL_DATETIME

print ("CSV to HDF")
bigcsv.powered_by_massa()

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "", [])
    except getopt.GetoptError as err:
        print str(err)
        print "Uso basico: python csv2hdf5.py"
        print "\t-h para ajuda"
        exit()

    csv_file_name = argv[0]
    param_file_name = os.path.splitext(csv_file_name)[0] + ".param"
    hdf_file_name = os.path.splitext(csv_file_name)[0] + ".h5"

    hdf = pd.HDFStore(hdf_file_name, 'w')
    print hdf

    #objects = dict((col,'object') for col in header)

    #dtype=objects,
    counter = 0
    for chunk in pd.read_csv(csv_file_name, header=0,
                             chunksize=100000):
        sys.stdout.write("\rLines: {lines:,}".format(lines=counter))
        sys.stdout.flush()
        hdf.append('df', chunk, min_itemsize=200)
        #print chunk
        counter += 1

    print hdf
    hdf.close()
    print ("\n*** FIM ***")


if __name__ == "__main__":
    main(sys.argv[1:])