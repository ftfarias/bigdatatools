# -*- coding: utf-8 -*-
# python ImportarFixedSizeFile.py -i OH53/AMD002.DST -s OH53/Import/Import_AMD002.txt -o AMD002.csv
# head -n 100 AMD002.csv > AMD002_head.csv

import sys
import getopt

colunas = {}
index_colunas = []
log_file = ""

def log(texto):
    print(texto)
    log_file.write(texto)

def parse_sas_structure(line):
    l = line.strip().split(" ")
    # log("Parseando %s " % l)
    if len(l) != 3:
        log("*** linha ignorada (poucos parametros): %s ***" % line)
        return None, 0, 0
    inicio = int(l[0][1:])-1  # O SAS indexa a primeira coluna como 1
    nome_coluna = l[1]
    tipo_coluna = l[2][:-1]
    tamanho = 0
    if '$' in tipo_coluna:
        #coluna String
        tamanho = int(tipo_coluna[1:])
    elif 'BEST' in  tipo_coluna:
        #coluna numerica
        tamanho = int(tipo_coluna[4:])
    elif 'YYMMDD' in  tipo_coluna:
        #coluna Data
        tamanho = int(tipo_coluna[6:])
    else:
        log("*** linha ignorada (tipo desconhecido): %s ***" % line)
        return "", 0, 0
    return nome_coluna, inicio, inicio+tamanho


def read_structure(struct_file):
    log("Colunas :")
    for line in struct_file:
        nome_coluna, inicio, fim = parse_sas_structure(line)
        if nome_coluna:
            index_colunas.append(nome_coluna)
            colunas[nome_coluna] = {}
            colunas[nome_coluna]['i'] = inicio
            colunas[nome_coluna]['f'] = fim
            log("Coluna %s, inicio na posição %d, tamanho %d, fim %d" %(nome_coluna,inicio,fim-inicio,fim))


def add_header(output_file):
    log("Adicionando HEADER:")
    header = ",".join(index_colunas)
    output_file.write(header)
    output_file.write("\n")
    log(header)


def parse_file(input_file, output_file_name, test_only):
    log("*** IMPORTANDO ***")
    if not test_only:
        output_file = open(output_file_name, 'w')
        add_header(output_file)

    for line_counter, line in enumerate(input_file):
        if test_only:
            for col in index_colunas:
                inicio = colunas[col]['i']
                fim = colunas[col]['f']
                valor = line[inicio:fim]
                log("(%d) Coluna %s, valor: [%s]" % (line_counter, col, valor))
            if line_counter >= 10:
                exit()
        else:
            l = [line[colunas[col]['i']:colunas[col]['f']].strip() for col in index_colunas]
            output_file.write(",".join(l))
            output_file.write("\n")
            if line_counter % 9876 == 0:
                #p.render(line_counter, "Linhas: %d" % line_counter)
                sys.stdout.write("\rImportado: %s" % '{0:,}'.format(line_counter))
                sys.stdout.flush()

    log("\n*** IMPORTAÇÃO TERMINADA ***")
    log("\nTotal importado: %s" % '{0:,}'.format(line_counter))
    if not test_only:
        output_file.close()


def main(argv):
    output_file_name = ""
    input_file_name = ""
    struct_file_name = ""
    test_only = False
    global log_file
    try:
        opts, args = getopt.getopt(argv, "thi:s:o:", ["help", "input-file=", "struct-file=", "output-file=", "test-only"])
    except getopt.GetoptError as err:
        print str(err)
        print "Uso basico: python ImportarFixedFile.py -i <input file> -s <struct file>"
        print "\t-h para ajuda"
        exit()
    for opt, arg in opts:
        if opt in ('-h', '-help'):
            print 'python ImportarFixedFile.py -i <input file> -s <struct file> -o <output-file>'
            print '-t : Mode teste, são listados os 10 primeiros registros'
            sys.exit()
        elif opt in ("-i", "--input-file"):
            input_file_name = arg
        elif opt in ("-s", "--struct-file"):
            struct_file_name = arg
        elif opt in ("-o", "--output-file"):
            output_file_name = arg
        elif opt in ("-t", "--test-only"):
            test_only = True

    print("Lendo arquivo %s, estrutura %s" % (input_file_name, struct_file_name))
    if test_only:
        print ("*** TESTE ***")

    log_file = open("log.txt", 'w')
    input_file = open(input_file_name, 'r')
    struct_file = open(struct_file_name, 'r')

    read_structure(struct_file)
    parse_file(input_file, output_file_name,test_only)

    input_file.close()
    struct_file.close()
    log_file.close()

    print ("\n*** FIM ***")

if __name__ == "__main__":
    main(sys.argv[1:])
