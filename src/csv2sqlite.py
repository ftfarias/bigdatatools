# -*- coding: utf-8 -*-

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import bigcsv
import csv
import argparse
import datetime
import ConfigParser
import sqlite3
import codecs

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter


print ("CSV Template")
bigcsv.powered_by_massa()

def main():
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('csv_file_name', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('table_name', help='Nome da tabela a ser criada')

    parser.add_argument('sqlite_file_name',
                        help='Nome do arquivo Sqlite')

    parser.add_argument('--param_file_name',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', '-u', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=100000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()

    column_types = []
    column_format = []

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.csv_file_name)[0] + ".param"

    if args.sqlite_file_name:
        sqlite_file_name = args.sqlite_file_name
    else:
        sqlite_file_name = os.path.splitext(args.csv_file_name)[0] + ".sqlite3"

    if not os.path.isfile(args.csv_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.csv_file_name))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.csv_file_name))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.csv_file_name))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    print('Abrindo Sqlite file: {file}'.format(file=sqlite_file_name))
    conn = sqlite3.connect(sqlite_file_name)

    print('Tabela: {table}'.format(table=args.table_name))

    header = param.get('MAIN', 'header').split("|")

    for i,c in enumerate(header):
        column_types.append(param.get(c, "column_type"))
        if param.has_option(c, 'column_format'):
            column_format.append(param.get(c, 'column_format'))
        else:
            column_format.append(None)

    campos = []
    for i, c in enumerate(header):
        field_type = bigcsv.bigcsv2sqlite[column_types[i]]
        campos.append("{name} {type} NULL".format(name=c,type=field_type))

    sql_droptable = "DROP TABLE {table}".format(table=args.table_name)

    sql_createtable = "CREATE TABLE IF NOT EXISTS {table} ( {fields} )".format(table=args.table_name, fields=",".join(campos))

    #c.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)', purchases)

    sql_insert = "insert into {table} values ({q})".format(table=args.table_name, q=','.join(['?']*len(header)))


    print ("Dropping table....")
    try:
        cur = conn.cursor()
        print sql_droptable;
        cur.execute(sql_droptable)
        conn.commit()
        print ("Table dropped !")
    except sqlite3.OperationalError as e:
        print(e)


    print ("Creating table....")
    cur = conn.cursor()
    print sql_createtable;
    cur.execute(sql_createtable)
    conn.commit()
    print ("Table created !")

    dialect = bigcsv.build_dialect_from_param(param)
    #bigcsv.print_dialect_params(dialect)

    counter = args.update_interval

    data_buffer = []

    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=int(param.get('MAIN', 'num_rows')))
    pbar.start()
    with bigcsv.smartopen(args.csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if param.get('MAIN', 'has_header') == 'True':
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            parsed_line = []
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval
                try:
                    cur.executemany(sql_insert, data_buffer)
                except Exception as err:
                   print ("Erro {e}: {s}".format(e=err, s=data_buffer))
                conn.commit()
                data_buffer = []
            for i, l in enumerate(line):
                coltype = column_types[i]
                if coltype == bigcsv.COLTYPE_STRING:
                    try:
                        s = codecs.decode(l, 'cp1252')
                        parsed_line.append(s)
                    except Exception as err:
                        print ("Erro {e} na coluna {i}: {s}".format(e=err, i=i, s=l))
                elif coltype == bigcsv.COLTYPE_INT:
                    try:
                        parsed_line.append(int(l))
                    except:
                        parsed_line.append(0)
                elif coltype == bigcsv.COLTYPE_LONG:
                    parsed_line.append(bigcsv.try_parse_long(l, 0))
                elif coltype == bigcsv.COLTYPE_FLOAT:
                    parsed_line.append(bigcsv.try_parse_float(l, 0))
                elif coltype == bigcsv.COLTYPE_BOOLEAN:
                    parsed_line.append(bigcsv.try_parse_boolean(l))
                elif coltype == bigcsv.COLTYPE_DATETIME:
                    parsed_line.append(bigcsv.try_parse_datetime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_DATE:
                    parsed_line.append(datetime.datetime.strptime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_TIME:
                    parsed_line.append(datetime.datetime.strptime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_NULL:
                    parsed_line.append(None)
                else:
                    print("Tipo de campo desconhecido: {c}".format(c=coltype))
            data_buffer.append(parsed_line)

    pbar.finish()
    cur.executemany(sql_insert, data_buffer)
    conn.commit()
    conn.close()
    print ("\n*** FIM ***")

if __name__ == "__main__":
    main()
