# -*- coding: utf-8 -*-
# para instalar o cx_Oracle,
# http://digitalsanctum.com/2007/07/26/installing-oracle-instant-client-on-mac-os-x/

import sys
#export DYLD_LIBRARY_PATH=/Applications/Postgres.app/Contents/Versions/9.3/lib/:/usr/local/oracle/instantclient

import cx_Oracle
import csv
import time
import os
import bigcsv

TABLE = sys.argv[1]
QUERY_SELECT_ALL = 'select * from %s'
CONN = 'ast_adm/hVYKKNFTBuvWGNH8YQMEUBfR6oB8tb@assinaturas-us.cift545shk7k.us-east-1.rds.amazonaws.com/orcl'

print ("Exportador Oracle")
bigcsv.powered_by_massa()

print 'Conectanto Oracle ...'
con = cx_Oracle.connect(CONN)
print 'Conectado Oracle versão %s' % con.version

file_name = "../dados/input/%s.full.csv" % TABLE
print("Exportando para o arquivo: %s" % file_name)

if os.path.isfile(file_name):
    print ("Arquivo CSV já existe! ")
    exit()

output = open(file_name, "w")
csv_writer = csv.writer(output, lineterminator="\n", quoting=csv.QUOTE_MINIMAL)

# export csv
cur = con.cursor()
# arraysize determina quantos registros sao lidos do banco de cada vez
cur.arraysize = 10000
query = QUERY_SELECT_ALL % TABLE
print("Executando query: {query}".format(query=query))
cur.execute(query)
print("Exportando...\n")
line_counter = 0
for row in cur:
    if line_counter == 0:
        csv_writer.writerow([col[0] for col in cur.description])
    csv_writer.writerow(row)
    line_counter += 1
    if line_counter % 10000 == 0:
        sys.stdout.write("\rImportada {linhas:,}".format(linhas=line_counter))
        sys.stdout.flush()
cur.close()
print("")
print("*** FIM ***")

# fecha as conexoes
con.close()
output.close()


