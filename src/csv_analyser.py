# -*- coding: utf-8 -*-

'''
    ['', '/Users/ftfarias/src/pyinstrument',
     '/Users/ftfarias/anaconda/lib/python27.zip',
     '/Users/ftfarias/anaconda/lib/python2.7',
     '/Users/ftfarias/anaconda/lib/python2.7/plat-darwin',
     '/Users/ftfarias/anaconda/lib/python2.7/plat-mac',
     '/Users/ftfarias/anaconda/lib/python2.7/plat-mac/lib-scriptpackages',
     '/Users/ftfarias/anaconda/lib/python2.7/lib-tk',
     '/Users/ftfarias/anaconda/lib/python2.7/lib-old',
     '/Users/ftfarias/anaconda/lib/python2.7/lib-dynload',
     '/Users/ftfarias/anaconda/lib/python2.7/site-packages',
     '/Users/ftfarias/anaconda/lib/python2.7/site-packages/PIL',
     '/Users/ftfarias/anaconda/lib/python2.7/site-packages/setuptools-3.4.4-py2.7.egg']
'''

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import ConfigParser
import csv
import bigcsv
from bigcsv import COLTYPE_STRING, COLTYPE_NULL, COLTYPE_INT, COLTYPE_LONG, \
    COLTYPE_BOOLEAN, COLTYPE_FLOAT, COLTYPE_DATE, COLTYPE_TIME, COLTYPE_DATETIME
import biglogging
import collections
import argparse

from progressbar import ProgressBar

import logging

#logging_path = os.path.join(os.getcwd(), 'log', os.path.splitext(sys.argv[0])[0])
logging_path = os.path.join(os.getcwd(), 'log')
biglogging.initialize_logger(logging_path)

logging.debug('*** START ***')


def copy_dialect_params(config, dialect):
    config.set("MAIN", "delimiter", dialect.delimiter)
    config.set("MAIN", "quotechar", dialect.quotechar)
    config.set("MAIN", "escapechar", dialect.escapechar)
    config.set("MAIN", "doublequote", dialect.doublequote)
    config.set("MAIN", "skipinitialspace", dialect.skipinitialspace)
    config.set("MAIN", "lineterminator", dialect.lineterminator.encode('string_escape'))
    config.set("MAIN", "quoting", dialect.quoting)

IDX_LENGTH = 0
IDX_NULL = 1
IDX_COUNTER = 2
IDX_MIN = IDX_TRUE = 3  # Min for int, TRUE for boolean
IDX_MAX = IDX_FALSE = 4


def guess_function(col_name, col_type, col_statistics):
    if col_statistics[IDX_COUNTER] is None:
        unique_values = sys.maxsize
    else:
        unique_values = len(col_statistics[IDX_COUNTER])
    logging.debug("Guessing function for column {col}, type {ty}, uniques:{u}".format(col=col_name,
                                                                                     ty=col_type,
                                                                                     u=unique_values))

    if col_type == COLTYPE_BOOLEAN:
        # boolean são sempre factors
        return bigcsv.COLFUNC_FACTOR

    votes = collections.Counter()

    if 'cod' in col_name.lower():
        votes.update([bigcsv.COLFUNC_KEY])

    if 'id' in col_name.lower():
        votes.update([bigcsv.COLFUNC_KEY])

    if 'key' in col_name.lower():
        votes.update([bigcsv.COLFUNC_KEY])

    if 'num' in col_name.lower():
        votes.update([bigcsv.COLFUNC_VALUE])

    if col_statistics[IDX_COUNTER] is not None:
        if unique_values <= 10:
                votes.update([bigcsv.COLFUNC_FACTOR])
                votes.update([bigcsv.COLFUNC_FACTOR])
        if unique_values <= 100:
                votes.update([bigcsv.COLFUNC_FACTOR])
    else:
        # tem mais de 1000 tipos:
        votes.update([bigcsv.COLFUNC_VALUE])
        votes.update([bigcsv.COLFUNC_KEY])

    if col_type == COLTYPE_STRING:
        votes.update([bigcsv.COLFUNC_KEY])
        votes.update([bigcsv.COLFUNC_FACTOR])

    elif col_type == COLTYPE_BOOLEAN:
        # boolean são sempre factors
        return bigcsv.COLFUNC_FACTOR

    elif col_type in [COLTYPE_FLOAT, COLTYPE_DATETIME, COLTYPE_DATE, COLTYPE_TIME]:
        # floats são sempre valores
        return bigcsv.COLFUNC_VALUE

    elif col_type in [COLTYPE_INT, COLTYPE_LONG]:
        if unique_values > 100:
            votes.update([bigcsv.COLFUNC_VALUE])
            votes.update([bigcsv.COLFUNC_KEY])
        else:
            votes.update([bigcsv.COLFUNC_FACTOR])

    logging.debug("Voting for column {col}: {v}".format(col=col_name, v=votes))

    winner = votes.most_common(1)
    logging.debug("winner for column {col}: {v}\n".format(col=col_name, v=winner))

    if not winner:
        winner = bigcsv.COLFUNC_KEY
    else:
        winner = winner[0][0]

    return winner

def main(input_file_name,
         param_file_name=None,
         update_interval=100000,
         sniff_size_dialect=100000,
         sniff_size_types=50000,
         verbose=1):

    columns_types = []
    columns_formats = {} # formato para colunas datetime
    column_statistics = []

    if not param_file_name:
        param_file_name = os.path.splitext(input_file_name)[0] + ".param"


    logging.info("Analysing file: " + input_file_name)
    logging.info("Creating PARAM file: " + param_file_name)

    param = ConfigParser.ConfigParser()
    if os.path.isfile(param_file_name):
        #logging.error("PARAM file already exists !!!! Aborting!")
        #logging.error('(If you want to recreate it, please delete with "rm {file}" first)'.format(file=parameters_file_name))
        #exit()
        logging.info("PARAM file exists!")
        param.read(param_file_name)
    else:
        param.add_section("MAIN")

    with bigcsv.smartopen(input_file_name, 'rb') as inputfile:
        ### sniffs the file to find the parsing parameters
        logging.info("Sniffing CSV for parsing parameters...")
        sample = inputfile.read(sniff_size_dialect)
        dialect = bigcsv.Sniffer().sniff(sample)
        bigcsv.print_dialect_params(dialect)
        copy_dialect_params(param, dialect)

    print('')
    logging.info("Starting parsing for header....")
    with bigcsv.smartopen(input_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        has_header = bigcsv.Sniffer().has_header(csv_reader)
        param.set("MAIN", "has_header", has_header)

        if has_header:
            logging.info("First line is header")
        else:
            logging.info("No header")

        ### check lines and columns

        # config.set("MAIN", "header", header)

    # counts the number of lines (total_rows) and the number of columns in each line
    logging.info("Counting lines and columns....")
    with bigcsv.smartopen(input_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)
        columns_freq = collections.Counter()
        counter = update_interval
        for line in csv_reader:
            counter -= 1
            if counter == 0:
                sys.stdout.write("\rLines: {lines:,}".format(lines=csv_reader.line_num))
                sys.stdout.flush()
                counter = update_interval
            columns_freq.update([len(line)])
        logging.info("\rTotal of rows: {total}".format(total=csv_reader.line_num))
        for c in columns_freq.keys():
            logging.info('Rows with {c} columns: {r}'.format(c=c, r=columns_freq[c]))
        param.set("MAIN", "column_len_freq", str(columns_freq))
        total_rows = csv_reader.line_num

        # guess how many columns the CSV file has
        max_values = bigcsv.max_values(columns_freq)
        if len(max_values) == 1:
            num_columns = max_values[0][0]
            logging.info("Number of columns: {col}".format(col=num_columns))
        else:
            logging.info("more than one possible column count...")
            for c in columns_freq:
                logging.info("{x} rows have {c} columns".format(c=c, x=columns_freq[c]))
            num_columns = max(max_values)
            logging.info("... so choosing largest value: {v} columns".format(v=num_columns))

        param.set("MAIN", "num_rows", total_rows)
        param.set("MAIN", "num_columns", num_columns)
        update_interval = min(total_rows/100, 100000)

    param.set("MAIN", "size", os.path.getsize(input_file_name))

    print('')
    logging.info("Starting parsing for types... (sniffing {c:,} rows)".format(c=sniff_size_types))
    with bigcsv.smartopen(input_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            header = csv_reader.next()
        else:
            header = ["C{i}".format(i=c) for c in xrange(len(num_columns))]

        param.set("MAIN", "header", "|".join(header))

        # add sections for config file
        for col in header:
            if not param.has_section(col):
                logging.debug('adding session {s} in param file '.format(s=col))
                param.add_section(col)
            columns_types.append("")

        # frequence counter for columns type
        columns_type_counter = []
        for i in xrange(num_columns):
            columns_type_counter.append(collections.Counter())

        # update the  sniff size if the file is too small
        sniff_size_types = min(sniff_size_types, total_rows)

        pbar = ProgressBar(widgets=bigcsv.widgets, maxval=sniff_size_types)
        pbar.start()
        counter = update_interval
        try:
            for line in csv_reader:
                counter -= 1
                if counter == 0:
                    pbar.update(csv_reader.line_num)
                    counter = update_interval
                if csv_reader.line_num >= sniff_size_types:
                    break
                for i, col in enumerate(line):
                    v = bigcsv.parse_type(col)
                    columns_type_counter[i].update(v)
        except csv.Error as e:
            sys.exit('Error at line %d: %s' % (csv_reader.line_num, e))
        pbar.finish()

    # print(column_max_length)

    for i, col in enumerate(header):
        counter = columns_type_counter[i]
        #print(counter)
        total_nulls = counter[COLTYPE_STRING]
        logging.debug("Types for column {i}:{col}: {ty}".format(i=i, col=col, ty=counter))
        column_type = bigcsv.guess_type_from_type_counts(counter, sniff_size_types)
        if COLTYPE_DATETIME in column_type or \
                COLTYPE_DATE in column_type or \
                COLTYPE_TIME in column_type:
            t, f = column_type.split("/")
            columns_formats[i] = f
            columns_types[i] = t
            param.set(col, "column_type", t)
            param.set(col, "column_format", f)
            #param.set(col, "column_measurement", m)
            logging.info("Column {col} has type {ty} with format {f}".format(col=col, ty=t, f=f))
        else:
            columns_types[i] = column_type
            param.set(col, "column_type", column_type)
            logging.info("Column {col} has type {ty}".format(col=col,ty=column_type))

        param.set(col, "column_function", bigcsv.COLFUNC_KEY)

        #for counter_key in list(counter):
        #    if counter_key != "NoneType":
        #        config.set(col, "contagem_"+bigcsv.TYPES_TO_STRING[counter_key], counter[counter_key])

    with open(param_file_name, "w+") as paramfile:
        param.write(paramfile)

    print('')
    logging.info("Full parsing for nulls and statistics.....")
    with bigcsv.smartopen(input_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        # skip header
        if has_header:
            csv_reader.next()

        # read first line
        col = csv_reader.next()

        # max length for columns
        for i in xrange(num_columns):
            if columns_types[i] == COLTYPE_INT:
                column_statistics.append([0, 0, collections.Counter(),
                                         bigcsv.try_parse_int(col[i], sys.maxint),
                                         bigcsv.try_parse_int(col[i], -sys.maxint)])

                try:
                    column_statistics[i][IDX_COUNTER].update_contact([int(col[i])])
                except Exception:
                    pass
            elif columns_types[i] == COLTYPE_LONG:
                column_statistics.append([0, 0, collections.Counter(),
                                         bigcsv.try_parse_long(col[i], sys.maxint),
                                         bigcsv.try_parse_long(col[i], -sys.maxint)])

                try:
                    column_statistics[i][IDX_COUNTER].update_contact([long(col[i])])
                except Exception:
                    pass
            elif columns_types[i] in [COLTYPE_FLOAT]:
                column_statistics.append([0, 0, collections.Counter(),
                                        bigcsv.try_parse_float(col[i], 1.0*sys.maxint),
                                        bigcsv.try_parse_float(col[i], -1.0*sys.maxint)])
                try:
                    column_statistics[i][IDX_COUNTER].update_contact([float(col[i])])
                except Exception:
                    pass

            elif columns_types[i] in [COLTYPE_BOOLEAN]:
                column_statistics.append([0, 0, None, 0, 0])

            else:
                column_statistics.append([0, 0, collections.Counter(), 0, 0])
                column_statistics[i][IDX_COUNTER].update([col[i]])


        logging.debug("First parsed:")
        for i, col in enumerate(header):
            logging.debug("Column {i}:{col} \t-> stats: {s}".format(i=i,col=col,s=column_statistics[i]))

        pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
        pbar.start()
        counter = update_interval
        try:
            for line in csv_reader:
                counter -= 1
                if counter == 0:
                    pbar.update(csv_reader.line_num)
                    counter = update_interval
                    # if a counter has more than 1000 different elements, stop counting it
                    for i in column_statistics:
                        if i[IDX_COUNTER] and len(i[IDX_COUNTER].items()) > 1000:  # more than 1000 diferrent elements
                            i[IDX_COUNTER] = None
                            logging.debug("Counter for column {i} has more than 1000 elements. Stop counting.".format(i=i))
                    #for i, col in enumerate(header):
                    #    logging.debug("Column {i}:{col} \t-> stats: {s}".format(i=i,col=col,s=column_statistics[i]))
                for i, col in enumerate(line):
                    # Tamanho
                    column_statistics[i][IDX_LENGTH] = max(column_statistics[i][IDX_LENGTH], len(col))

                    if columns_types[i] == bigcsv.COLTYPE_INT:
                        try:
                            v = int(col)
                            column_statistics[i][IDX_MIN] = min(column_statistics[i][IDX_MIN], v)
                            column_statistics[i][IDX_MAX] = max(column_statistics[i][IDX_MAX], v)
                            c = column_statistics[i][IDX_COUNTER]
                            if c is not None:
                                c.update([v])
                        except ValueError as e:
                            column_statistics[i][IDX_NULL] += 1


                    elif columns_types[i] == bigcsv.COLTYPE_LONG:
                        try:
                            v = long(col)
                            column_statistics[i][IDX_MIN] = min(column_statistics[i][IDX_MIN], v)
                            column_statistics[i][IDX_MAX] = max(column_statistics[i][IDX_MAX], v)
                            c = column_statistics[i][IDX_COUNTER]
                            if c is not None:
                                c.update_contact([v])
                        except ValueError:
                            column_statistics[i][IDX_NULL] += 1

                    elif columns_types[i] == bigcsv.COLTYPE_FLOAT:
                        try:
                            v = float(col)
                            column_statistics[i][IDX_MIN] = min(column_statistics[i][IDX_MIN], v)
                            column_statistics[i][IDX_MAX] = max(column_statistics[i][IDX_MAX], v)
                            c = column_statistics[i][IDX_COUNTER]
                            if c is not None:
                                c.update([v])
                        except ValueError:
                            column_statistics[i][IDX_NULL] += 1

                    elif columns_types[i] == bigcsv.COLTYPE_BOOLEAN:
                        v = bigcsv.parse_boolean(col)
                        if v is None:
                            column_statistics[i][IDX_NULL] += 1
                        elif v:
                            column_statistics[i][IDX_TRUE] += 1
                        else:
                            column_statistics[i][IDX_FALSE] += 1

                    elif columns_types[i] == bigcsv.COLTYPE_DATETIME:
                        c = column_statistics[i][IDX_COUNTER]
                        if c is not None:
                            c.update([col])
                    else:
                        c = column_statistics[i][IDX_COUNTER]
                        if c is not None:
                            c.update([col])

        except csv.Error as e:
            sys.exit('Error at line %d: %s' % (csv_reader.line_num, e))
        pbar.finish()

        logging.debug("Parsed results:")
        for i, col in enumerate(header):
            logging.debug("Column {i}:{col}\t{s}".format(i=i,col=col,s=column_statistics[i]))

        for i, col in enumerate(header):
            param.set(col, "max_length", column_statistics[i][IDX_LENGTH])
            param.set(col, "nulls", column_statistics[i][IDX_NULL])
            param.set(col, "nulls_%", 1.0*column_statistics[i][IDX_NULL]/total_rows)
            if columns_types[i] == bigcsv.COLTYPE_BOOLEAN:
                total = column_statistics[i][IDX_TRUE]+column_statistics[i][IDX_FALSE]
                param.set(col, "true_values", column_statistics[i][IDX_TRUE])
                param.set(col, "true_%", 1.0*column_statistics[i][IDX_TRUE]/total)
                param.set(col, "false_value", column_statistics[i][IDX_FALSE])
                param.set(col, "false_%", 1.0*column_statistics[i][IDX_FALSE]/total)
            else:
                if columns_types[i] in [bigcsv.COLTYPE_INT, bigcsv.COLTYPE_LONG, bigcsv.COLTYPE_FLOAT]:
                    param.set(col, "min_value", column_statistics[i][IDX_MIN])
                    param.set(col, "max_value", column_statistics[i][IDX_MAX])

                if column_statistics[i][IDX_COUNTER] is not None:
                    param.set(col, "top20", column_statistics[i][IDX_COUNTER].most_common(20))
                    param.set(col, "uniques_values", len(column_statistics[i][IDX_COUNTER].items()))
                else:
                    param.set(col, "top20", "<more then 1000 unique values>")
                    param.set(col, "uniques_values", "<more then 1000 unique values>")

    print("Guessing columns funtions...")
    for i, col in enumerate(header):
        col_function = guess_function(col, columns_types[i], column_statistics[i])
        logging.info("Column {col} has function {f}".format(col=col, f=col_function))
        param.set(col, "column_function", col_function, )

    logging.debug('Saving PARAM file')
    with open(param_file_name, "w+") as paramfile:
        param.write(paramfile)
    logging.debug('PARAM file saved')

    logging.info("*** END ***")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('csv_file_name', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('--param_file_name',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', '-u', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=100000)

    parser.add_argument('--sniff_size_types', '-st', type=int,
                        help='Quantos registros devem ser examinados para determinação do tipo. Default = 50000',
                        default=50000)

    parser.add_argument('--sniff_size_dialect', '-sd', type=int,
                        help='Quantos registros devem ser examinados para determinação do dialeto. Default = 100000',
                        default=100000)

    parser.add_argument('--verbose', '-v', action='count', default=1,
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()

    print ("CSV Analyser")
    bigcsv.powered_by_massa()

    main(args.csv_file_name,
         param_file_name=args.param_file_name,
         update_interval=args.update_interval,
         sniff_size_types=args.sniff_size_types,
         sniff_size_dialect=args.sniff_size_dialect,
         verbose=args.verbose)

'''
- colocar tipo de campo: factor (e já totalizar)
- colocar se o campo pode estar em branco e a linha ainda ser válida,
    ou se este campo invalida a linha toda
- Separa NULL/em branco de ERRO
- Colocar uma sessão "comentarios"
    - uniques com < de 10 casos

'''
