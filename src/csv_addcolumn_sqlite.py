# -*- coding: utf-8 -*-

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import bigcsv
import csv
import argparse
import ConfigParser
import sqlite3

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter


def main():
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('template_file', help='Arquivo template para nova coluna')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()

    print ("CSV AddColumn")
    bigcsv.powered_by_massa()

    if not os.path.isfile(args.template_file):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.template_file))
        exit()

    conn_tmp_dados_raiz = sqlite3.connect('../dados/working/modelo.sqlite3')
    sqlite_cursor = conn_tmp_dados_raiz.cursor()

    sqlite_cursor.execute("PRAGMA synchronous = OFF")
    sqlite_cursor.execute("PRAGMA journal_mode = MEMORY")

    template = ConfigParser.ConfigParser()
    template.read(args.template_file)

    input_csv = template.get('ADDCOLUMN', 'input_csv')
    output_csv = template.get('ADDCOLUMN', 'output_csv')
    input_key_column = template.get('ADDCOLUMN', 'input_key_column')
    insert_pos = int(template.get('ADDCOLUMN', 'insert_pos'))
    sql = template.get('ADDCOLUMN', 'sql')

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(input_csv)[0] + ".param"

    if not os.path.isfile(input_csv):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=input_csv))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=input_csv))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=input_csv))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)

    print ("Total de registros: {total:,}".format(total=total_rows))

    if input_key_column not in header:
        print ("input_key_column {i} nao encontrada".format(i=input_key_column))
        exit()

    print("Carregando tabela: "+sql)
    counter = 0
    data = {}
    sqlite_cursor = sqlite_cursor.execute(sql)
    for row in sqlite_cursor:
        if counter == 0:
            sql_header = [col[0] for col in sqlite_cursor.description]
        counter += 1
        if counter % 100000 == 0:
            sys.stdout.write("\rLines: {lines:,}".format(lines=counter))
            sys.stdout.flush()
        data[str(row[0])] = row[1:]
    print("\n")

    print("Foram lidos {total:,} registos.".format(total=len(data)))

    print('Criando arquivo de saida CSV: {file}'.format(file=output_csv))
    output_file = open(output_csv, 'w')
    output_csv_writer = csv.writer(output_file, dialect=dialect)

    counter = args.update_interval
    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
    pbar.start()
    idx_csv_key_column = header.index(input_key_column)
    print('Index da coluna com a chave: {idx}'.format(idx=idx_csv_key_column))
    bigcsv.column_print(columns_params[input_key_column])

    empty_data = ['']*(len(sql_header)-1)

    key_found = 0
    with bigcsv.smartopen(input_csv, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            header = csv_reader.next()
            for h in sql_header[1:]:
                header.insert(insert_pos, h)
            output_csv_writer.writerow(header)

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval

            #print (len(line))
            #print (idx_csv_key_column)
            chave_csv = line[idx_csv_key_column]
            #print("chave:"+chave_csv)
            try:
                data_to_insert = data[chave_csv]
                key_found += 1
            except KeyError:
                data_to_insert = empty_data
            #print('-> {x}   '.format(x = data_to_insert))
            for h in data_to_insert:
                line.insert(insert_pos, h)
            output_csv_writer.writerow(line)

    pbar.finish()
    conn_tmp_dados_raiz.close()
    print ("Foram encontradas {found:,} chaves de {total:,}: {p}%".format(found=key_found, total=total_rows,
                                                                         p=100.0*key_found/total_rows)  )
    print ("\n*** FIM ***")

if __name__ == "__main__":
    main()