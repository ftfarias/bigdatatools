# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
import os
import bigutil
from prettytable import PrettyTable

# fix path for Pypy

sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))
'''
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/setuptools-3.4.4-py2.7.egg'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-dynload'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-old'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-tk'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-mac'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-darwin'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-mac/lib-scriptpackages'))
'''

import bigcsv
import csv
import argparse
import ConfigParser
import collections
import scipy.stats as stats
import numpy as np

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter

def compare_columns(col1, cp1, col2, cp2):
    if cp1.function == bigcsv.COLFUNC_FACTOR and cp2.function == bigcsv.COLFUNC_FACTOR:
        compare_columns_factor_factor(col1, cp1, col2, cp2)
    else:
        print("todo")


def compare_columns_factor_factor(col1, cp1, col2, cp2):
    frequence = collections.Counter()
    for i in xrange(len(col1)):
        frequence.update([(col1[i], col2[i])])

    print(frequence)

    #http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.chi2_contingency.html

    obs = np.array([[frequence[(True,  True)], frequence[(True,  False)]],
                    [frequence[(False, True)], frequence[(False, False)]]])

    chi2, p, dof, ex = stats.chi2_contingency(obs)

    print("Observations:")
    x = PrettyTable(["", "True", "False"])
    x.add_row(["True"]+list(obs[0]))
    x.add_row(["False"]+list(obs[1]))
    print(x)

    #print("Expected:")
    #print(ex)

    print("")
    x = PrettyTable(["Metrica", "Value", "Interpretation"])
    x.add_row(["Chi2", chi2, "Chi2 value"])
    x.add_row(["p-value", p, "if p < 0.05, coluns are CORRELATED"])
    x.add_row(["Degrees of freedom", dof, ""])
    print(x)


def main():
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('csv_file_name', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('coluna1', help='Nome da coluna 1')
    parser.add_argument('coluna2', help='Nome da coluna 2')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()

    print ("Columns")
    bigcsv.powered_by_massa()

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.csv_file_name)[0] + ".param"

    if not os.path.isfile(args.csv_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.csv_file_name))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.csv_file_name))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.csv_file_name))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)
    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)

    col1_name = args.coluna1
    col2_name = args.coluna2

    if col1_name not in header:
        print("{w} not found.... did you mean '{p}' ?".format(w=col1_name,p=bigutil.find_most_similar(col1_name, header)))
        exit()

    if col2_name not in header:
        print("{w} not found.... did you mean '{p}' ?".format(w=col2_name,p=bigutil.find_most_similar(col2_name, header)))
        exit()

    col_param1 = columns_params[col1_name]
    col_param2 = columns_params[col2_name]
    bigcsv.column_print(col_param1)
    bigcsv.column_print(col_param2)

    dialect = bigcsv.build_dialect_from_param(param)
    #bigcsv.print_dialect_params(dialect)

    counter = args.update_interval

    total_rows = int(param.get('MAIN', 'num_rows'))
    print ("Total of rows: {total:,}".format(total=total_rows))

    columns = bigcsv.read_columns([args.coluna1, args.coluna2],
                                  columns_params,
                                  total_rows,
                                  args.csv_file_name,
                                  has_header,
                                  args.update_interval,
                                  dialect,
                                  remove_nones=False)

    compare_columns(columns[0], col_param1, columns[1], col_param2)

    """
    param.set(col1_name, '_{col}_chi2'.format(col=col2_name), chi2)
    param.set(col1_name, '_{col}_chi2_p-value'.format(col=col2_name), p)

    param.set(col2_name, '_{col}_chi2'.format(col=col1_name), chi2)
    param.set(col2_name, '_{col}_chi2_p-value'.format(col=col1_name), p)

    with open(param_file_name, "w+") as paramfile:
        param.write(paramfile)
    """

    print("\n*** FIM ***")

if __name__ == "__main__":
    main()