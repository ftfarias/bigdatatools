import sqlite3

oracle_conn = sqlite3.connect('modelo_tmp_dados_raiz.sqlite3')
c = oracle_conn.cursor()

c.execute('SELECT * FROM tmp_dados_raiz limit 1')
c.fetchone()

for i,col in enumerate(c.description):
    print "{i} -> {col}".format(i=i,col=col[0])



# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
oracle_conn.close()

