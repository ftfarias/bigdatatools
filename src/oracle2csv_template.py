# -*- coding: utf-8 -*-
# para instalar o cx_Oracle,
# http://digitalsanctum.com/2007/07/26/installing-oracle-instant-client-on-mac-os-x/

import sys
#export DYLD_LIBRARY_PATH=/Applications/Postgres.app/Contents/Versions/9.3/lib/:/usr/local/oracle/instantclient
# python oracle2csv_template.py imports/assinatura_periodo_idade.import $RETENCAO_DATA_DIR

import cx_Oracle
import csv
import time
import os
import bigcsv
import ConfigParser

template_file = sys.argv[1]

data_dir = sys.argv[2]

template = ConfigParser.ConfigParser()
template.read(template_file)

query = template.get('IMPORT', 'query')
file_name = os.path.join(data_dir, template.get('IMPORT', 'file'))

#QUERY_SELECT_ALL = 'select * from %s where ROWNUM < 100000 order by COD_ASSINANTE, COD_PROJETO, QTD_RENOVACAO'
CONN = 'ast_adm/hVYKKNFTBuvWGNH8YQMEUBfR6oB8tb@assinaturas-us.cift545shk7k.us-east-1.rds.amazonaws.com/orcl'

print ("Exportador Oracle")
bigcsv.powered_by_massa()

print 'Conectanto Oracle ...'
con = cx_Oracle.connect(CONN)
print 'Conectado Oracle versão %s' % con.version

print("Exportando para o arquivo: %s" % file_name)

if os.path.isfile(file_name):
    print ("Arquivo CSV já existe! ")
    exit()

output = open(file_name, "w")
csv_writer = csv.writer(output, lineterminator="\n", quoting=csv.QUOTE_MINIMAL)

# export csv
cur = con.cursor()
# arraysize determina quantos registros sao lidos do banco de cada vez
cur.arraysize = 10000
print("Executando query: {query}".format(query=query))
cur.execute(query)
print("Exportando...\n")
line_counter = 0
for row in cur:
    if line_counter == 0:
        csv_writer.writerow([col[0] for col in cur.description])
    csv_writer.writerow(row)
    line_counter += 1
    if line_counter % 10000 == 0:
        sys.stdout.write("\rImportada {linhas:,}".format(linhas=line_counter))
        sys.stdout.flush()
cur.close()
print("")
print("*** FIM ***")

# fecha as conexoes
con.close()
output.close()


