# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
import os
import bigutil
from prettytable import PrettyTable

# fix path for Pypy

sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))
'''
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/setuptools-3.4.4-py2.7.egg'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-dynload'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-old'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-tk'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-mac'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-darwin'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-mac/lib-scriptpackages'))
'''

import bigcsv
import csv
import argparse
import ConfigParser
import collections
import scipy.stats as stats
import numpy as np

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter


def read_column(col_idx, col_type, col_format, total_rows, csv_file_name, has_header, update_interval, dialect):
    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
    pbar.start()
    result = []
    counter = update_interval
    with bigcsv.smartopen(csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            parsed_line = []
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = update_interval

            col_value = bigcsv.to_type(line[col_idx], col_type, col_format)
            if col_value is not None:
                result.append(col_value)

    pbar.finish()
    print("Column loaded, size: {s:,} kbytes".format(s=sys.getsizeof(result)/1024))
    return result


def main():
    parser = argparse.ArgumentParser(description='CSV Basic Statistics')

    parser.add_argument('csv_file_name', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('coluna', help='Nome da coluna')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()

    print ("Stats Basic")
    bigcsv.powered_by_massa()

    column_types = []
    column_format = []

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.csv_file_name)[0] + ".param"

    if not os.path.isfile(args.csv_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.csv_file_name))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.csv_file_name))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.csv_file_name))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    header = param.get('MAIN', 'header').split("|")
    print (header)

    col_name = args.coluna

    if args.coluna not in header:
        print("{w} not found.... did you mean '{p}' ?".format(w=col_name, p=" or ".join(bigutil.find_similars(col_name, header))))
        exit()

    col_idx = header.index(col_name)

    for i, c in enumerate(header):
        column_types.append(param.get(c, "column_type"))
        if param.has_option(c, 'column_format'):
            column_format.append(param.get(c, 'column_format'))
        else:
            column_format.append(None)

    col_type = column_types[col_idx]
    col_format = column_format[col_idx]

    if column_format is not None:
        print("Column {col} at index {idx}, type {ty}, format {f}".format(col=col_name, idx=col_idx, ty=col_type, f=col_format))
    else:
        print("Column {col} at index {idx}, type {ty}".format(col=col_name, idx=col_idx, ty=col_type))


    dialect = bigcsv.build_dialect_from_param(param)
    #bigcsv.print_dialect_params(dialect)

    counter = args.update_interval

    total_rows = int(param.get('MAIN', 'num_rows'))
    print ("Total of rows: {total:,}".format(total=total_rows))

    has_header = param.get('MAIN', 'has_header') == 'True'

    v = read_column(col_idx, col_type, col_format, total_rows, args.csv_file_name, has_header, args.update_interval, dialect)
    values = np.asarray(v)
    print(type(values))
    print (values)
    v_len, v_minmax, v_mean, v_variance, v_skewness, v_kurtosis= stats.describe(v)


    mean_cntr, var_cntr, std_cntr = stats.bayes_mvs(v,alpha=0.9)
    print("Bayesian Mean (lower, upper) at 90% confidence: {m} ({l}, {u}) ".format(m=mean_cntr[0],
                                                                                   l=mean_cntr[1][0],
                                                                                   u=mean_cntr[1][1]))
    mean_cntr, var_cntr, std_cntr = stats.bayes_mvs(v,alpha=0.95)
    print("Bayesian Mean (lower, upper) at 95% confidence: {m} ({l}, {u}) ".format(m=mean_cntr[0],
                                                                                   l=mean_cntr[1][0],
                                                                                   u=mean_cntr[1][1]))
    mean_cntr, var_cntr, std_cntr = stats.bayes_mvs(v, alpha=0.99)
    print("Bayesian Mean (lower, upper) at 99% confidence: {m} ({l}, {u}) ".format(m=mean_cntr[0],
                                                                                   l=mean_cntr[1][0],
                                                                                   u=mean_cntr[1][1]))

    print("Variance: {m} ".format(m=var_cntr))


    param.set(col_name,'numpy_len', v_len)
    param.set(col_name,'numpy_min', v_minmax[0])
    param.set(col_name,'numpy_max', v_minmax[1])
    param.set(col_name,'numpy_mean', v_mean)
    param.set(col_name,'numpy_variance', v_variance)
    param.set(col_name,'numpy_std_dev', np.sqrt(v_variance))
    param.set(col_name,'numpy_skewness', v_skewness)
    param.set(col_name,'numpy_kurtosis', v_kurtosis)

    #with open(param_file_name, "w+") as paramfile:
    #    param.write(paramfile)

    print("\n*** FIM ***")

if __name__ == "__main__":
    main()