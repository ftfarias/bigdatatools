# -*- coding: utf-8 -*-

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import bigcsv
import csv
import argparse
import ConfigParser
from progressbar import ProgressBar

#
# python csv_filtra_registros.py ../dados/input/vw_tmpdadosraiz_ultimos2anos.10k.csv x.csv COD_PRODUTO=901
#


def main(args):
    filter_columns = []
    filter_columns_idx = []
    filter_values = []
    filter_op = []

    def test_filters(line):
        for i, c in enumerate(filter_columns_idx):
            #print(">"+line[i]+"<")
            #print(filter_values[c])
            #print("if line[c]({0}) not in filter_values[i]({1}):".format(line[c],filter_values[i]))
            if not line[c]:
                return False

            elif filter_op[i] == "in":
                if line[c] not in filter_values[i]:
                    return False

            elif filter_op[i] == "notin":
                if line[c] in filter_values[i]:
                    return False

            elif filter_op[i] == "=":
                if line[c] != filter_values[i]:
                    return False

            elif filter_op[i] == "!=":
                if line[c] == filter_values[i]:
                    return False

            else:
                raise ValueError('Parser error: No OP')

        return True

    def create_filter(f):
        f = f.strip()
        if '#in#' in f:
            t = f.split('#in#')
            if len(t) != 2:
                raise ValueError('Parser erro: Could not parse filter:'+f)
            filter_columns.append(t[0])
            filter_op.append("in")
            filter_values.append(t[1].split(","))
        elif '#notin#' in f:
            t = f.split('#notin#')
            if len(t) != 2:
                raise ValueError('Parser erro: Could not parse filter:'+f)
            filter_columns.append(t[0])
            filter_op.append("notin")
            filter_values.append(t[1].split(","))
        elif '=' in f:
            t = f.split('=')
            if len(t) != 2:
                raise ValueError('Parser erro: Could not parse filter:'+f)
            filter_columns.append(t[0])
            filter_op.append("=")
            filter_values.append(t[1])
        elif '!=' in f:
            t = f.split('!=')
            if len(t) != 2:
                raise ValueError('Parser erro: Could not parse filter:'+f)
            filter_columns.append(t[0])
            filter_op.append("!=")
            filter_values.append(t[1])
        else:
            raise ValueError('Parser erro: Could not parse filter:'+f)

    print ("Filtrar Registros")
    bigcsv.powered_by_massa()

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.input_csv)[0] + ".param"

    if not os.path.isfile(args.input_csv):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.input_csv))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.input_csv))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.input_csv))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)
    #print(columns_params)

    output_file = open(args.output_csv, 'w')
    output_csv_writer = csv.writer(output_file, dialect=dialect)
    output_csv_writer.writerow(header)

    print("Filtro str: "+args.filtro)
    filters = args.filtro.split("?")
    print("Filtros: "+str(filters))
    # create filters
    for f in filters:
        create_filter(f)

    filter_columns_idx = [header.index(c) for c in filter_columns]

    print("**** DEBUG ***")
    print(filter_columns)
    print(filter_columns_idx)
    print(filter_values)
    print(filter_op)
    print("**** DEBUG ***")


    print("Processando colunas:")
    for i in xrange(len(filter_columns)):
        print("\t{i}: Coluna {c}, indice {idx}".format(i=i,c=filter_columns[i], idx=filter_columns_idx[i]))

    print("Total of rows: {total:,}".format(total=total_rows))

    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
    pbar.start()
    counter = args.update_interval
    output_rows = 0
    with bigcsv.smartopen(args.input_csv, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            parsed_line = []
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval

            if test_filters(line):
                output_csv_writer.writerow(line)
                output_rows += 1


    pbar.finish()
    output_file.close()
    print("Registros de entrada: {0}  registros de saida: {1}".format(total_rows,output_rows))
    print ("\n*** FIM ***")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('input_csv', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('output_csv', help='Nome do arquivo de saidas')

    parser.add_argument('filtro', help='filtro')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=100000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.1.0')

    args = parser.parse_args()
    main(args)