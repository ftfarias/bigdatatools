# -*- coding: utf-8 -*-

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import bigcsv
import csv
import argparse
import ConfigParser
import collections
from progressbar import ProgressBar


def main(args):
    print ("Count Uniques")
    bigcsv.powered_by_massa()

    campos = args.columns.replace(",", "_")

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.csv_file_name)[0] + ".param"

    if args.latex_file_name:
        latex_file_name = args.latex_file_name
    else:
        file_name = os.path.splitext(os.path.basename(args.csv_file_name))[0]
        latex_file_name = '../documentacao/' + file_name + "_" + campos + ".tex"

    if args.csv_contagem_file_name:
        csv_contagem_file_name = args.csv_contagem_file_name
    else:
        file_name = os.path.splitext(os.path.basename(args.csv_file_name))[0]
        csv_contagem_file_name = '../dados/working/' + file_name + "_" + campos + ".csv"

    if not os.path.isfile(args.csv_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.csv_file_name))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.csv_file_name))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.csv_file_name))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))

    print('Arquivo Latex: {file}'.format(file=latex_file_name))
    print('Arquivo CSV com contagens: {file}'.format(file=csv_contagem_file_name))

    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)
    #print(columns_params)

    print(header)
    filter_columns = args.columns.split(",")
    filter_column_idx = [header.index(c) for c in filter_columns]

    print("Processando colunas:")
    for i in xrange(len(filter_columns)):
        print("\t{i}: Coluna {c}, indice {idx}".format(i=i,c=filter_columns[i], idx=filter_column_idx[i]))

    print ("Total de linhas: {total:,}".format(total=total_rows))

    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
    pbar.start()
    counter = args.update_interval
    group_by = collections.Counter()
    key = []
    with bigcsv.smartopen(args.csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            parsed_line = []
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval


            key = [line[x] for x in filter_column_idx]
            group_by.update(["|".join(key)])

            # constructs the line with the correct types
            # for idx in filter_column_idx:
            #     v = line[idx]
            #     # parse the value to the right type
            #     value = bigcsv.to_type(v, columns_params[header[idx]])
            #     # add to the parsed line
            #     parsed_line.append(value)

            # Do the job here
            #print parsed_line
    pbar.finish()

    for v in group_by.most_common(len(group_by)):
        print("{0:10}: {1}".format(v[0],v[1]))

    output_file = open(csv_contagem_file_name,"w")
    output_file.write(",".join(filter_columns))
    output_file.write(",contagem")
    output_file.write("\n")
    for v in group_by.most_common(len(group_by)):
        s = v[0].split("|")+[str(v[1])]
        output_file.write(",".join(s))
        output_file.write("\n")
    output_file.close()

    def format_line(line):
        if line == '':
            line = '<em branco>'
        line = line.replace("&", "\&")
        line = line.replace("_", "\_")
        return line


    # cria o arquivo Latex
    output_file = open(latex_file_name,"w")
    output_file.write("\\begin{center}\n")
    output_file.write("\\begin{{tabular}}{{| {0}c |}}\n".format("c | "*len(filter_columns)))
    output_file.write("\\hline\n")
    output_file.write("\n")
    # titulo
    columns = ["\\textbf{"+format_line(x)+"}" for x in filter_columns]
    s = columns + ["contagem"]
    line = " & ".join(s)
    output_file.write(line)
    output_file.write("\\\\ \\hline \n")
    # dados
    total = len(group_by)
    count_listados = 0
    for v in group_by.most_common(20):
        count_listados += 1
        columns = [ format_line(x) for x in v[0].split("|")]
        s = columns + ["{0:,}".format(v[1])]
        line = " & ".join(s)
        output_file.write(line)
        output_file.write("\\\\ \\hline \n")
    output_file.write("\\end{tabular}\n")
    output_file.write("\\end{center}\n")
    output_file.write("Listando as {0} maiores contagens de {1} total\n".format(count_listados, total))
    output_file.close()

    print ("\n*** FIM ***")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('csv_file_name', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('columns', help='Nome das colunas a serem contadas')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--latex_file_name', nargs='?',
                        help='Nome do arquivo latex.',
                        default=None)

    parser.add_argument('--csv_contagem_file_name', nargs='?',
                        help='Nome do arquivo csv com a contagem.',
                        default=None)


    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()
    main(args)