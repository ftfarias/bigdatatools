# -*- coding: utf-8 -*-

import sys
import os
import cPickle as pickle
import bigcsv
import csv
import argparse
import datetime
import ConfigParser

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter


def full_scan(csv_file_name, key, has_header, total_rows, dialect, columns_idx):
    result = []
    counter = args.update_interval
    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=total_rows)
    pbar.start()
    with bigcsv.smartopen(csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if has_header:
            header = csv_reader.next()

        for line in csv_reader:
            counter -= 1
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = args.update_interval

            index_join = "|".join([line[i] for i in columns_idx])

            if index_join == key:
                result.append(line)

    pbar.finish()
    return result


def key_scan(csv_file_name, key, indexes, dialect):
    result = []

    print("Procurando o indice....")
    pos = indexes[key]

    print("Procurando os dados....")
    with bigcsv.smartopen(csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)
        for p in pos:
            inputfile.seek(p, 0)
            result.append(csv_reader.next())

    return result


def main(args):
    print ("CSV Join")
    bigcsv.powered_by_massa()

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.input_csv)[0] + ".param"

    if not os.path.isfile(args.input_csv):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.input_csv))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.input_csv))
        exit()

    if args.param_file_name:
        idx_file_name = args.idx_file_name
    else:
        idx_file_name = os.path.splitext(args.input_csv)[0] + ".idx"

    if not os.path.isfile(idx_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=idx_file_name))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.input_csv))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)
    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)

    print ("Total of rows: {total:,}".format(total=total_rows))

    if args.full_scan:
        columns_idx = [header.index(c) for c in args.key_columns.split(",")]
        result = full_scan(args.input_csv, args.key, has_header, total_rows, dialect, columns_idx)
    else:
        print('Abrindo arquivo IDX: {file}'.format(file=idx_file_name))

        with open(idx_file_name, "rb") as idx_file:
            indexs = pickle.load(idx_file)

        print("index size: {0} MB".format(sys.getsizeof(indexs)/(1024*1024)))

        print('Indices carregados')
        result = key_scan(args.input_csv, args.key, indexs, dialect)

    print (result)

    print ("\n*** FIM ***")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('input_csv', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('key', help='Chave a ser procurada')

    parser.add_argument('--key_columns', nargs='?', help='Colunas do arquivo input que servem de chave')

    parser.add_argument('--idx_file_name', nargs='?',
                        help='Nome do arquivo IDX. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")


    parser.add_argument('--full_scan', '-s', nargs='?',
                        help="Força um full scan")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()
    main(args)
