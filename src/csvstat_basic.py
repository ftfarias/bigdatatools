# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
import os
from prettytable import PrettyTable

# fix path for Pypy

sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))
'''
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/setuptools-3.4.4-py2.7.egg'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-dynload'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-old'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/lib-tk'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-mac'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-darwin'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/plat-mac/lib-scriptpackages'))
'''

import bigcsv
import csv
import argparse
import ConfigParser
import collections
import scipy.stats as stats
import numpy as np

PERCENTILES = [p*5 for p in range(0, 20)]

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter

def main():
    parser = argparse.ArgumentParser(description='CSV Basic Statistics')

    parser.add_argument('csv_file_name', help='Nome do arquivo CSV a ser processado')

    parser.add_argument('--param_file_name', nargs='?',
                        help='Nome do arquivo PARAM. Por default é o mesmo do arquivo CSV',
                        default=None)

    parser.add_argument('--update_interval', nargs='?', type=int,
                        help='Intervalo de atualização da barra de progresso',
                        default=10000)

    parser.add_argument('--verbose', '-v', action='count',
                        help="Mostra mais informações. Use vários -v para maior nivel de debug")

    parser.add_argument('--version', action='version', version='1.0.0')

    args = parser.parse_args()

    print ("Stats Basic")
    bigcsv.powered_by_massa()

    if args.param_file_name:
        param_file_name = args.param_file_name
    else:
        param_file_name = os.path.splitext(args.csv_file_name)[0] + ".param"

    if not os.path.isfile(args.csv_file_name):
        print ("Arquivo {file} não foi encontrado... Tem certeza que ele existe ?".format(file=args.csv_file_name))
        exit()

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py {file}"'.format(file=args.csv_file_name))
        exit()

    print('Abrindo arquivo CSV: {file}'.format(file=args.csv_file_name))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)

    print ("Total of rows: {total:,}".format(total=total_rows))

    #col_names = [col_name for col_name in header if columns_params[col_name].type in [bigcsv.COLTYPE_INT,
    #                                                                                  bigcsv.COLTYPE_LONG,
    #                                                                                  bigcsv.COLTYPE_FLOAT]]

    col_names = [col_name for col_name in header if columns_params[col_name].function in [bigcsv.COLFUNC_VALUE] \
                and columns_params[col_name].type in [bigcsv.COLTYPE_INT, bigcsv.COLTYPE_LONG, bigcsv.COLTYPE_FLOAT]]

    print ("Loading columns columns: {cols}".format(cols=','.join(col_names)))

    columns = bigcsv.read_columns(col_names,
                                  columns_params,
                                  total_rows,
                                  args.csv_file_name,
                                  has_header,
                                  args.update_interval,
                                  dialect,
                                  remove_nones=True)

    for i, col_name in enumerate(col_names):
        v = columns[i]
        print('--------------------------------')
        bigcsv.column_print(columns_params[col_name])

        v_len, v_minmax, v_mean, v_variance, v_skewness, v_kurtosis = stats.describe(v)

        print("Mean:     {m} ".format(m=v_mean))
        print("Variance: {m} ".format(m=v_variance))
        print("Std_Dev:  {m} ".format(m=np.sqrt(v_variance)))
        print("Skewness: {m} ".format(m=v_skewness))
        print("Kurtosis: {m} ".format(m=v_kurtosis))

        param.set(col_name,'numpy_len', v_len)
        param.set(col_name,'numpy_%_valid_values', 1.0*v_len/total_rows)
        param.set(col_name,'numpy_min', v_minmax[0])
        param.set(col_name,'numpy_max', v_minmax[1])
        param.set(col_name,'numpy_mean', v_mean)
        param.set(col_name,'numpy_variance', v_variance)
        param.set(col_name,'numpy_std_dev', np.sqrt(v_variance))
        param.set(col_name,'numpy_skewness', v_skewness)
        param.set(col_name,'numpy_kurtosis', v_kurtosis)

        mean_cntr, var_cntr, std_cntr = stats.bayes_mvs(v, alpha=0.95)
        print("Bayesian Mean (lower, upper) at 95% confidence: {m} ({l}, {u}) ".format(m=mean_cntr[0],
                                                                                       l=mean_cntr[1][0],
                                                                                       u=mean_cntr[1][1]))

        param.set(col_name,'numpy_Bayesian_mean_lower_95', mean_cntr[1][0])
        param.set(col_name,'numpy_Bayesian_mean_upper_95', mean_cntr[1][1])
        param.set(col_name,'numpy_Bayesian_std_lower_95', std_cntr[1][0])
        param.set(col_name,'numpy_Bayesian_std_upper_95', std_cntr[1][1])

        '''
        mean_cntr, var_cntr, std_cntr = stats.bayes_mvs(v, alpha=0.99)
        print("Bayesian Mean (lower, upper) at 99% confidence: {m} ({l}, {u}) ".format(m=mean_cntr[0],
                                                                                       l=mean_cntr[1][0],
                                                                                       u=mean_cntr[1][1]))
        param.set(col_name,'numpy_Bayesian_mean_lower_99', mean_cntr[1][0])
        param.set(col_name,'numpy_Bayesian_mean_upper_99', mean_cntr[1][1])
        param.set(col_name,'numpy_Bayesian_std_lower_99', std_cntr[1][0])
        param.set(col_name,'numpy_Bayesian_std_upper_99', std_cntr[1][1])
        '''

        k2, p_value = stats.normaltest(v)

        param.set(col_name, 'numpy_normaltest', k2)
        param.set(col_name, 'numpy_normaltest_p_value', p_value)
        if p_value < 0.05:
            param.set(col_name,'numpy_normaltest_result', 'NOT normally distributed')
        else:
            param.set(col_name,'numpy_normaltest_result', 'IS normally distributed')

        w, p_value = stats.shapiro(v)

        param.set(col_name,'numpy_Shapiro_Wilk', w)
        param.set(col_name,'numpy_Shapiro_Wilk_p_value', p_value)
        if p_value < 0.05:
            param.set(col_name,'numpy_Shapiro_Wilk_result', 'NOT normally distributed')
        else:
            param.set(col_name,'numpy_Shapiro_Wilk_result', 'IS normally distributed')

        percentil = np.percentile(v, PERCENTILES)
        param.set(col_name, 'percentiles', percentil)

        print('\n')

    with open(param_file_name, "w+") as paramfile:
        param.write(paramfile)

    print("\n*** FIM ***")

if __name__ == "__main__":
    main()