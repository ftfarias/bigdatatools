# -*- coding: utf-8 -*-

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import bigcsv
import csv
import getopt
import datetime
import ConfigParser
import shelve

from progressbar import ProgressBar, Bar, Timer, AdaptiveETA, FileTransferSpeed, Percentage, Counter


print ("CSV Template")
bigcsv.powered_by_massa()

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "", [])
    except getopt.GetoptError as err:
        print str(err)
        print "Uso basico: python .py"
        print "\t-h para ajuda"
        exit()

    column_types = []
    column_format = []

    csv_file_name = argv[0]
    param_file_name = os.path.splitext(csv_file_name)[0] + ".param"
    shelve_file_name = os.path.splitext(csv_file_name)[0] + ".shelve"

    print('Abrindo arquivo CSV: {file}'.format(file=csv_file_name))

    print('Abrindo arquivo Param: {file}'.format(file=param_file_name))
    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    print('Abrindo arquivo Shelve: {file}'.format(file=shelve_file_name))
    db = shelve.open(shelve_file_name)

    header = param.get('MAIN', 'header').split("|")

    for i,c in enumerate(header):
        column_types.append(param.get(c, "column_type"))
        if param.has_option(c, 'column_format'):
            column_format.append(param.get(c, 'column_format'))
        else:
            column_format.append(None)

    dialect = bigcsv.build_dialect_from_param(param)
    #bigcsv.print_dialect_params(dialect)

    key_fields = ['COD_CLI_CRM','COD_PROD_CD_CDMS','NUM_EDICAO_INI']
    key_idx = [header.index(k) for k in key_fields]

    update_interval = 10000
    counter = update_interval

    pbar = ProgressBar(widgets=bigcsv.widgets, maxval=int(param.get('MAIN', 'num_rows')))
    pbar.start()
    with bigcsv.smartopen(csv_file_name, 'rb') as inputfile:
        csv_reader = csv.reader(inputfile, dialect=dialect)

        if param.get('MAIN', 'has_header') == 'True':
            csv_reader.next()  # skip header

        for line in csv_reader:
            counter -= 1
            parsed_line = []
            if counter == 0:
                pbar.update(csv_reader.line_num)
                counter = update_interval
            for i, l in enumerate(line):
                coltype = column_types[i]
                if coltype == bigcsv.COLTYPE_STRING:
                    parsed_line.append(l)
                elif coltype == bigcsv.COLTYPE_INT:
                    try:
                        parsed_line.append(int(l))
                    except:
                        parsed_line.append(0)
                elif coltype == bigcsv.COLTYPE_LONG:
                    parsed_line.append(bigcsv.try_parse_long(l, 0))
                elif coltype == bigcsv.COLTYPE_FLOAT:
                    parsed_line.append(bigcsv.try_parse_float(l, 0))
                elif coltype == bigcsv.COLTYPE_BOOLEAN:
                    parsed_line.append(bigcsv.try_parse_boolean(l))
                elif coltype == bigcsv.COLTYPE_DATETIME:
                    parsed_line.append(datetime.datetime.strptime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_DATE:
                    parsed_line.append(datetime.datetime.strptime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_TIME:
                    parsed_line.append(datetime.datetime.strptime(l, column_format[i]))
                elif coltype == bigcsv.COLTYPE_NULL:
                    parsed_line.append(None)
                else:
                    print("Tipo de campo desconhecido: {c}".format(c=coltype))
            print parsed_line
            key = "|".join([str(parsed_line[k]) for k in key_idx])
            print key
            db[key] = parsed_line

    pbar.finish()
    db.close()
    print ("\n*** FIM ***")

if __name__ == "__main__":
    main(sys.argv[1:])
