import numpy as np
import sys
import pandas as pd

from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import StratifiedShuffleSplit


data_raw = pd.read_csv(sys.argv[1], low_memory=False)
labels = data_raw.SAIDA_RENOVADO_E_CONFIRMADO.as_matrix()
train = []
test = []
sss = StratifiedShuffleSplit(labels, 1, test_size=0.2, random_state=42)
for train_index, test_index in sss:
    train = train_index
    test = test_index
train_output = open(sys.argv[2] + 'treino_ids_' + sys.argv[3] + '.txt', 'w')
test_output = open(sys.argv[2] + 'teste_ids_' + sys.argv[3] + '.txt', 'w')
for i in train:
    train_output.write(str(i) + ',' + str(labels[i]) + '\n')
for i in test:
    test_output.write(str(i) + ',' + str(labels[i]) + '\n')
train_output.close()
test_output.close()
