# -*- coding: utf-8 -*-

import sys
import os

# fix path for Pypy
sys.path.insert(0, os.path.expanduser(''))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages'))
sys.path.insert(0, os.path.expanduser('~/anaconda/lib/python2.7/site-packages/PIL'))

import bigcsv
import csv
import argparse
import ConfigParser


def main(args):
    print ("Param 2 Latex")
    bigcsv.powered_by_massa()

    param_file_name = args.param_file_name

    file_name = os.path.splitext(os.path.basename(param_file_name))[0]
    output_file_name = '../documentacao/' + file_name + ".tex"

    print ("Gerando arquivo Latex: {file}".format(file=output_file_name))

    if not os.path.isfile(param_file_name):
        print ("Arquivo de parametro {file} não foi encontrado.".format(file=param_file_name))
        print ('Caso necessário, crie usando "pypy csv_analyser.py"')
        exit()


    param = ConfigParser.ConfigParser()
    param.read(param_file_name)

    dialect, total_rows, has_header, header, columns_params = bigcsv.read_columns_from_param(param)

    output_file = open(output_file_name,"w")

    def p(t):
        output_file.write(t.replace("%", "\\%"))
        #print(t)

    p("\\textbf{Sumário:}\n")
    p("\\begin{itemize}  \\itemsep1pt \\parskip1pt \\parsep1pt\n")
    p("\\item \\textsc{{Tamanho (bytes):}} {s:,}\n".format(s=int(param.get("MAIN", "size"))))
    p("\\item \\textsc{{Total de linhas:}} {l:,}\n".format(l=total_rows))
    p("\\item \\textsc{{Atributos:}} {a}\n".format(a=len(header)))
    p("\\end{itemize}")

    p("\\textbf{Atributos:}\n")
    p("\\begin{multicols}{2}")
    p("\\begin{enumerate}[leftmargin=*] \\itemsep1pt \\parskip1pt \\parsep1pt\n")
    for c in columns_params:
        cp = columns_params[c]
        col = cp.name
        #print(col)
        p("     \\item  \\textbf{{{c}}}\n".format(c=col.replace("_", "\\_")))
        p("     \\begin{itemize}\n")
        p("         \\item \\textsc{{Tipo de dado:}} {0}\n".format(cp.column_type))
        p("         \\item \\textsc{{Função:}} {0}\n".format(cp.function))
        p("         \\item \\textsc{{Tamanho máximo:}} {0} caracteres\n".format(param.get(col, "max_length")))
        nulls = int(param.get(col, "nulls"))
        nulls_pp = float(param.get(col, "nulls_%"))*100
        p("         \\item \\textsc{{Em branco:}} {c:,} ({pp:.0f}%)\n".format(c=nulls, pp=nulls_pp))
        if cp.format is not None:
            p("         \\item \\textsc{{Formato:}} {0}\n".format(cp.format))
        p("     \\end{itemize}\n")
    p("\\end{enumerate}\n")
    p("\\end{multicols}")

    # min_value = 1544
    # max_value = 14521360
    # top20 = <more then 1000 unique values>
    # uniques_values = <more then 1000 unique values>

    output_file.close()
    print ("\n*** FIM ***")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CSV Template')

    parser.add_argument('param_file_name', help='Nome do arquivo Param a ser processado')

    args = parser.parse_args()
    main(args)